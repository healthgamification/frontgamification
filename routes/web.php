<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::namespace('Auth')->group(function () {  
  //Route::get('/','LoginController@showLoginForm');
  Route::get('/login','LoginController@showLoginForm');
  Route::post('login', 'LoginController@login')->name('login');
  Route::get('/logout','LoginController@logout')->name('logout');  
// Route::post('/password/email',"ForgotPasswordController@sendResetLinkEmail")->name("password.email");
// Route::get('/password/reset',"ForgotPasswordController@showLinkRequestForm")->name("password.request");
// Route::post('/password/reset',"ForgotPasswordController@reset")->name("password.update");
// Route::get('/password/reset/{token}',"ForgotPasswordController@showResetForm")->name("password.reset");
 });

Auth::routes();
Route::middleware(['auth'])->group(function () {
	Route::group(['prefix' => '{locale?}','middleware' => 'setlocale'], function() {
		Route::get('/', 'DashboardController@index');
		Route::get('/dashboard', 'DashboardController@getIndex')->name('dashboard');
		Route::get('/info', 'DashboardController@info')->name('info');
		Route::get('/my-group', 'DashboardController@getMygroup')->name('mygroup');
		Route::get('/group', 'DashboardController@getGroup')->name('group');
		Route::get('/quiz', 'DashboardController@getQuiz')->name('quiz');
		Route::get('/challenge', 'DashboardController@getChallenge')->name('challenge');
		Route::get('/point-table', 'DashboardController@getPointTable')->name('pointtable');
		Route::get('/edit-profile', 'DashboardController@getEdit')->name('edit');
		Route::post('/edit-profile', 'DashboardController@postEdit')->name('pedit');
		Route::post("/teampost",'DashboardController@postTeamPost')->name('tpedit');
	});
	
		Route::post("/getquestion",'Question@getquestion')->name('getquestion');
		Route::post("/save","Question@saveQuestion")->name("question.save");
		Route::post("/savechallenge","DashboardController@saveChallenge")->name("challenge.save");
		Route::post("/savetodaychallenge","DashboardController@saveTodayChallenge")->name("challenge.savetoday");
		Route::get("/quizzresult/{qid}/{weekno}","Question@getResult")->name("quizz.result");
		Route::post("/check",'Question@checkAnswer')->name('quizz.checkanswer');
});


