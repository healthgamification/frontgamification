<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\QuestionSet;
use App\PlayerQuestion;
use App\PlayerAnswer;
use App\Question as questionmodal;
use App\Answers;
use App\Team_player;
use Auth;

class Question extends Controller
{
    //

	public function getquestion(Request $request)
	{
		$questions = QuestionSet::find($request->quizz_id)->questions;
		$questionwithans=array();
		foreach ($questions as $key => $question) {
			$questionwithans[$key]['question'] = $question->question->question;
			$questionwithans[$key]['question_id'] = $question->question->id;
			$questionwithans[$key]['answers'] = $question->question->answers->pluck("option","id")->all();
		}
		return response($questionwithans, 200)
		->header('Content-Type', 'application/json');  
	}

	public function saveQuestion(Request $request)
	{

		$team = Team_player::where(['memebr_id'=>Auth::user()->id,'organization_id'=>Auth::user()->organization_id])->orderBy('id','DESC')->first();
		if($request->is_result=='0')
		{
		$oldquestion = questionmodal::find($request->question_id);
		$oldquestionset = QuestionSet::find($request->quizz_id);
		$answer = Answers::where(["question_id"=>$oldquestion->id,"is_correct"=>'1'])->first();

		$category=array();
		foreach ($oldquestion->categories as $key => $c) {
			array_push($category,$c->category->category_name);
		}
		$count = PlayerQuestion::where(["qsetid"=>$request->quizz_id,"organization_id"=>Auth::user()->organization_id,'team_id'=>$team->team_id,"memebr_id"=>Auth::user()->id,"question_id"=>$oldquestion->id,"week_no"=>$request->weekno])->count();

		if($request->question_no=='0' && $count==0)
		{

			foreach ($oldquestionset->questions as $key => $q) {
				$answer_first = Answers::where(["question_id"=>$q->question->id,"is_correct"=>'1'])->first();
				$category=array();
				foreach ($q->question->categories as $key => $c) {
					array_push($category,$c->category->category_name);
				}
				$question = new PlayerQuestion();
				$question['category_name']=json_encode($category);
				$question['qsetid']=$oldquestionset->id;
				$question['question_id']=$q->question->id;
				$question['week_no']=$request->weekno;
				$question['qsettitle']=$oldquestionset->title;
				$question['qsetdescription']=$oldquestionset->description;
				$question['question']=$q->question->question;
				$question['lang']=$q->question->lang;
				$question['organization_id']=Auth::user()->organization_id;
				$question['team_id']=$team->team_id;
				$question['memebr_id']=Auth::user()->id;
				$question['weightage']=$q->question->weightage;
				$question['correct_answer']=$answer_first->id;
				$question['user_answer']=null;
				$question['is_correct']=2;
				$question['response_time']=0;
				$question['status']="Active";
				$question->save();

				foreach ($q->question->answers as  $ans) {
					$player_answer = new PlayerAnswer();
					$player_answer['question_id']=$q->question->id;
					$player_answer['answer_id']=$ans->id;
					$player_answer['option']=$ans->option;
					$player_answer['is_correct']=$ans->is_correct;
					$player_answer['status']=$ans->status;
					$player_answer->save();
				}
			}
		}
		if($request->answer_id)
		{
		$totalweightage = array_sum($oldquestionset->questions->pluck("question")->pluck("weightage")->all())*10;
		$obtained_mark=(($oldquestion->weightage*10)*100)/$totalweightage;
		$question_up_data['week_no']=$request->weekno;
		$question_up_data['correct_answer']=$answer->id;
		$question_up_data['user_answer']=$request->answer_id;
		$question_up_data['is_correct']=($request->answer_id!=''?($answer->id==$request->answer_id?1:0):2);
		$question_up_data['obtained_mark']=$question_up_data['is_correct']==1?$obtained_mark:0;
		$question_up_data['response_time']=$request->response_time;

		
		PlayerQuestion::where(["qsetid"=>$request->quizz_id,"organization_id"=>Auth::user()->organization_id,'team_id'=>$team->team_id,"memebr_id"=>Auth::user()->id,"question_id"=>$oldquestion->id])->update($question_up_data);
	}

		}else{
			$redata = PlayerQuestion::where(["qsetid"=>$request->quizz_id,"organization_id"=>Auth::user()->organization_id,'team_id'=>$team->team_id,"memebr_id"=>Auth::user()->id]);
			$totalobtained=0;
			foreach ($redata->get() as $question) {
				$totalobtained += $question->obtained_mark;

			}
			$html ='<div class="col-md-12">
			<table class="table">
			<thead>
			  <tr>
				<th>'.translate("quizz_result_summry_txt").'</th>
				<th>|</th>
				<th>'.translate("quizz_result_result_txt").'</th>
			  </tr>	
			</thead>			  
			<tr>
				<td><strong>'.translate("quizz_result_total_question_txt").'</strong></td>
				<td>|</td>
				<td>'.$redata->count().'</td>
			</tr>
			<tr>
				<td><strong>'.translate("quizz_result_attempt_question_txt").'</strong></td>
				<td>|</td>
				<td>'.$redata->where("is_correct","<>",2)->count().'</td>
			</tr>
			<tr>
				<td><strong>'.translate("quizz_result_correct_question_txt").'</strong></td>
				<td>|</td>
				<td>'.$redata->where("is_correct",1)->count().'</td>
			</tr>
			<tr>
				<td><strong>'.translate("quizz_result_total_marks_txt").'</strong></td>
				<td>|</td>
				<td>100</td>
			</tr>
			<tr>
				<td><strong>'.translate("quizz_result_obtained_marks_txt").'</strong></td>
				<td>|</td>
				<td>'.round($totalobtained).'</td>
			</tr>
			<tr>
				<td><strong>'.translate("quizz_result_percentage_txt").'</strong></td>
				<td>|</td>
				<td>'.((round($totalobtained)*100)/100).'%</td>
			</tr>
			</table>
			</div>';
			return response($html, 200)
			->header('Content-Type', 'text/html');  

		}
		return response("Data saved", 200)
		->header('Content-Type', 'application/json');  
	}

	public function getResult($qid,$weekno)
	{
		$team = Team_player::where(['memebr_id'=>Auth::user()->id,'organization_id'=>Auth::user()->organization_id])->orderBy('id','DESC')->first();
		$redata = PlayerQuestion::where(["qsetid"=>$qid,"organization_id"=>Auth::user()->organization_id,'team_id'=>$team->team_id,"memebr_id"=>Auth::user()->id,"week_no"=>$weekno]);
		//dd($redata->get());
			$totalobtained=0;
			foreach ($redata->get() as $question) {
				$totalobtained += $question->obtained_mark;

			}
			$html ='<div class="col-md-12">
			<table class="table">
			<thead>
			  <tr>
				<th>'.translate("quizz_result_summry_txt").'</th>
				<th>|</th>
				<th>'.translate("quizz_result_result_txt").'</th>
			  </tr>	
			</thead>			  
			<tr>
				<td><strong>'.translate("quizz_result_total_question_txt").'</strong></td>
				<td>|</td>
				<td>'.$redata->count().'</td>
			</tr>
			<tr>
				<td><strong>'.translate("quizz_result_attempt_question_txt").'</strong></td>
				<td>|</td>
				<td>'.$redata->where("is_correct","<>",2)->count().'</td>
			</tr>
			<tr>
				<td><strong>'.translate("quizz_result_correct_question_txt").'</strong></td>
				<td>|</td>
				<td>'.$redata->where("is_correct",1)->count().'</td>
			</tr>
			<tr>
				<td><strong>'.translate("quizz_result_total_marks_txt").'</strong></td>
				<td>|</td>
				<td>100</td>
			</tr>
			<tr>
				<td><strong>'.translate("quizz_result_obtained_marks_txt").'</strong></td>
				<td>|</td>
				<td>'.round($totalobtained).'</td>
			</tr>
			<tr>
				<td><strong>'.translate("quizz_result_percentage_txt").'</strong></td>
				<td>|</td>
				<td>'.((round($totalobtained)*100)/100).'%</td>
			</tr>
			</table>
			</div>';
			return response($html, 200)
			->header('Content-Type', 'text/html');  
	}

	public function checkAnswer(Request $request)
	{
		$answer = Answers::where(["question_id"=>$request->question_id,"is_correct"=>'1'])->first();
		$question = questionmodal::find($request->question_id);
		$fact= $question->fact??"Nice quizz.";
		return response(["correct"=>$answer->id,"fact"=>$fact], 200)
			->header('Content-Type', 'application/json');  
	}
}
