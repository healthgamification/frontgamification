<?php

namespace App\Http\Controllers;

use App\PlayerModel;
use Illuminate\Http\Request;
use DB;
use Auth;
use App\Team;
use App\TeamPost;
use Lang;
use Hash;
use App\Team_player;
use App\QuestionSet;
use App\AssignQuizWeekModel;
use App\AssignWeekModel;
use App\PlayerQuestion;
use App\AssignChallengeWeekModel;
use App\PlayerChallengeStart;
use App\PlayerChallenge;
use App\PlayerChallengeOption;
use App\ChallengeOption;
use App\Challenge;
use Illuminate\Support\Facades\Schema;
use App\DynamicField;
use App\EmployeeDetails;
class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $perpage = 5;

    public function index()
    {
        //
       // return view("dashboard.dashboard");
        return view("index");
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function getIndex()
    {
        //
        return view("dashboard.dashboard");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PlayerModel  $playerModel
     * @return \Illuminate\Http\Response
     */
    public function show(PlayerModel $playerModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PlayerModel  $playerModel
     * @return \Illuminate\Http\Response
     */
    public function edit(PlayerModel $playerModel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PlayerModel  $playerModel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PlayerModel $playerModel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PlayerModel  $playerModel
     * @return \Illuminate\Http\Response
     */
    public function destroy(PlayerModel $playerModel)
    {
        //
    }


    public function info()
    {
        $user = Auth::user();
        $org = DB::table("organizations")->where("id",$user->organization_id)->first();
         return view("info",compact('org'));
    }

    public function getEdit()
    {
        $user = Auth::user();
        $profileimgs=array('avatar.png','avtar_1.png','avtar_3.png','avtar_4.png','avtar_5.png','avtar_6.png','avtar_7.png','avtar_8.png','avtar_9.png','avtar_10.png','avtar_11.png','avtar_12.png','avtar_13.png','avtar_14.png','avtar_15.png','avtar_16.png','avtar_17.png','avtar_18.png','avtar_19.png','user-2.png','user-3.png','user-4.png','user-5.png','user-6.png','user-7.png');
        $profile_info=array();
        $lang_code = \Session::get("lang_code");
        if(Schema::hasTable('dynamic_fields')){
          $profile_info = DynamicField::where(["status"=>"Active","lang_code"=>$lang_code])->orderBy("priority","asc")->get();
        }
         return view("edit-profile",compact('profileimgs','user','profile_info'));
    }

    public function postEdit(Request $request)
    {
        $this->validate($request,[
            'profile_image'=>'required',
            'member_name'=>'required',
            'current_password'=>'required_with_all:password',
            'password'=>'confirmed',
            'password_confirmation'=>'required_with_all:password',
        ]);

        $player = PlayerModel::find(Auth::id());
        if(!empty($request->current_password) && !empty($request->password) && !empty($request->password_confirmation))
        {
            //if(Hash::make($request->current_password) == $player->password )
            if(Hash::check($request->current_password, $player->password))
            {                
                $player->password=Hash::make($request->password);
            }else{
                 return response(['errors'=>['current_password'=>'Current Password not match.']], 403)
                  ->header('Content-Type', 'application/json');
            }
        }
        $lang_code = \Session::get("lang_code");

        $player->member_name= $request->member_name;
        $player->avatar = $request->profile_image;
        $player->save();

        if(!empty($request->profile_info)){
            foreach (DynamicField::where(["status"=>"Active","lang_code"=>$lang_code])->get() as $field) {
                $indata=array(
                    "field_id"=>$field->id,
                    "title"=>$field->label,
                    "value"=>is_array($request->profile_info[$field->name])?json_encode($request->profile_info[$field->name]):$request->profile_info[$field->name],
                    "member_id"=>\Auth::id()                    
                );
                EmployeeDetails::updateOrCreate(["field_id"=>$field->id,"member_id"=>\Auth::id()],$indata);
            }
        }      

        return response($player, 200)
                  ->header('Content-Type', 'application/json');
    }

    public function getMygroup()
    {
        $id = Auth::id();
        $orgid = Auth::user()->organization_id;
        $team = Team_player::where(['memebr_id'=>$id,'organization_id'=>$orgid,"status"=>"Active"])->orderBy('id','DESC')->first();
        $team_id = $team->team_id;
        $myteam = Team::find($team_id);
        $date=date("Y-m-d");
        $week = AssignWeekModel::where("status","Active")
        ->where("startdate","<=",$date)
        ->where("enddate",">=",$date)
        ->where("org_id",$orgid)
        ->first();
        if(!$week)
        {
             $week = AssignWeekModel::where("status","Active")
             ->where("startdate",">",$date)
        ->where("org_id",$orgid)->orderBy("id","asc")
        ->first();
        }
        $week_no = $week?$week->id:0;
        $challengeweek = AssignChallengeWeekModel::where(["org_id"=>$orgid,"week_id"=>$week_no])->get();
        $challengesets = $challengeweek->pluck("challengesets");
        $challengemapping = $challengesets->pluck("challengemapping");
         $members=Team_player::where(['team_id'=>$team_id,'organization_id'=>$orgid,"status"=>"Active"])->get()->pluck("member");
       $allchallenge = array_sum($members->pluck("no_of_challenge")->all());
       $allquizz =  array_sum($members->pluck("no_of_quizzes")->all());
       $allplayedquizz = count(PlayerQuestion::where(["organization_id"=>$orgid,"week_no"=>$week_no,"team_id"=>$team_id])->groupBy(["memebr_id","qsetid"])->get()->pluck("organization_id")->all());
       $allplayedchallenge = count(PlayerChallenge::where(["organization_id"=>$orgid,"week_no"=>$week_no,"team_id"=>$team_id])->groupBy(["member_id","challenge_id"])->get()->pluck("organization_id")->all());
        return view('my-group',compact('myteam','allchallenge','allplayedchallenge','allplayedquizz','allquizz'));
    }

    public function getGroup()
    {
        $id = Auth::id();
        $orgid = Auth::user()->organization_id;
        $team = Team_player::where(['memebr_id'=>$id,'organization_id'=>$orgid])->orderBy('id','DESC')->first();
        $team_id = $team->team_id;

        $posts = TeamPost::where("status","Active")
        ->where("organization_id",$orgid)
        ->where("team_id",$team_id)
        ->orderBy("created_at","desc")->paginate($this->perpage);
        return view('group',compact('posts'));
    }

    public function postTeamPost(Request $request)
    {
        $this->validate($request,[
            "team_post"=>"required|string"
        ]);
        $team_id = Team_player::where("memebr_id", Auth::id())->first()->team_id;

        $tpost = new TeamPost();
        $tpost->organization_id=Auth::user()->organization_id;
        $tpost->team_id=$team_id;
        $tpost->member_id = Auth::id();
        $tpost->post =$request->team_post;
        $tpost->status ="Active";
        $tpost->created_at = date('Y-m-d H:i:s');
        $tpost->save();
        $msg = __(Lang::locale().'.TEAM_POST_SAVE_MSG');
        return response(['msg'=>$msg], 200)
                  ->header('Content-Type', 'application/json');        
    }

    public function getQuiz()
    {
        $date=date("Y-m-d");        
        $orgid = Auth::user()->organization_id;
        $week = AssignWeekModel::where("status","Active")
        ->where("startdate","<=",$date)
        ->where("enddate",">=",$date)
        ->where("org_id",$orgid)
        ->first();
         $quizzes=array();
         $totalplayed=0;
        if(!$week)
        {
             $week = AssignWeekModel::where("status","Active")
             ->where("startdate",">",$date)
        ->where("org_id",$orgid)->orderBy("id","asc")
        ->first();
        }else{
        $week_no = $week?$week->id:0;
        $quizzes = AssignQuizWeekModel::where(["org_id"=>$orgid,"week_id"=>$week_no])->get();
         $team_id = Team_player::where("memebr_id", Auth::id())->orderBy("id","desc")->first()->team_id;
        $totalplayed = count(PlayerQuestion::where(["organization_id"=>$orgid,"week_no"=>$week_no,"memebr_id"=>Auth::user()->id,"team_id"=>$team_id])->groupBy("qsetid")->get()->pluck("qsetid")->all());
    }
        return view('quiz',compact('quizzes','totalplayed','week'));
    }

    public function getChallenge()
    {
        $date=date("Y-m-d");
        $orgid = Auth::user()->organization_id;
        $week = AssignWeekModel::where("status","Active")
        ->where("startdate","<=",$date)
        ->where("enddate",">=",$date)
        ->where("org_id",$orgid)
        ->first();
        $challengs=array();
        $totalplayed=0;
        $week_no=0;
        if(!$week)
        {
             $week = AssignWeekModel::where("status","Active")
             ->where("startdate",">",$date)
        ->where("org_id",$orgid)->orderBy("id","asc")
        ->first();
        }else{
        $week_no = $week?$week->id:0;
        $challengeweek = AssignChallengeWeekModel::where(["org_id"=>$orgid,"week_id"=>$week->id])->get();
        $challengesets = $challengeweek->pluck("challengesets");
        $challengemapping = $challengesets->pluck("challengemapping");
        $challengs=array();
        foreach ($challengemapping as $key => $mappting) {
            foreach ($mappting->pluck("challenges") as $key => $ch) {
               array_push($challengs, $ch);
           }
        }
      // $challengs = isset($challengemapping[0])?$challengemapping->pluck("challenges"):array();
         $team_id = Team_player::where("memebr_id", Auth::id())->orderBy("id","desc")->first()->team_id;
        $totalplayed = PlayerChallengeStart::where(["organization_id"=>$orgid,"week_no"=>$week_no,"member_id"=>Auth::user()->id,"team_id"=>$team_id])->count();
    }
        return view('challenge', compact('challengs','totalplayed','week_no','week'));
    }


public function getPointTable()
    {
         $orgid = Auth::user()->organization_id;
        $teams = Team::where(["organization_id"=>$orgid,"status"=>"Active"])->orderBy("name",'asc')->get()->sortBy(function($team){
            return -$team->point;
        });
         $team_id = Team_player::where("memebr_id", Auth::id())->orderBy("id","desc")->first()->team_id;
        return view('point-table',compact('teams','team_id'));
    }

public function saveChallenge(Request $request)
{
    $this->validate($request,[
        "challenge_id"=>"required",
    ]);
    $user_id = Auth::user()->id;
    $organization_id = Auth::user()->organization_id;
    $team_id = Team_player::where(['memebr_id'=>$user_id,'organization_id'=>$organization_id])->orderBy('id','DESC')->first()->team_id;
    $week = AssignWeekModel::where(["id"=>$request->week_no,"org_id"=>$organization_id])->first();
    $totalmark=100;
    if($request->option && $request->option!='')
    {
    $seloption = ChallengeOption::find($request->option);
    $totalmark=$seloption->point;
    if($totalmark==0)
    {
        $totalmark=100;
    }
}

    $oldchallenge = Challenge::find($request->challenge_id);
    $challengs = new PlayerChallenge();
    $challengs->category_name=$oldchallenge->category->name;
    $challengs->csetid=$oldchallenge->setmap->set->id;
    $challengs->challenge_id=$request->challenge_id;
    $challengs->week_no=$week->id;
    $challengs->csettitle=$oldchallenge->setmap->set->title;
    $challengs->csetdescription=$oldchallenge->setmap->set->description;
    $challengs->challenge=$oldchallenge->challenge_name;
    $challengs->challenge_desc=$oldchallenge->challenge_description;
    $challengs->lang=$oldchallenge->lang_code;
    $challengs->organization_id=$organization_id;
    $challengs->team_id=$team_id;
    $challengs->member_id=$user_id;
    $challengs->complexity_of_challenge=$oldchallenge->complexity_of_challenge;
    $challengs->no_of_days_to_complete=$oldchallenge->no_of_days_to_complete;
    $challengs->obtained_mark=($totalmark/$oldchallenge->no_of_days_to_complete);
    $challengs->selected_option=$request->option??0;
    $challengs->status=$oldchallenge->challenge_status;
    $challengs->save();
    if($oldchallenge->options->count()>0)
    {
        foreach ($oldchallenge->options as $option) {            
                $challenge_option = new PlayerChallengeOption();
                $challenge_option->challenge_id =$option->challenge_id;
                $challenge_option->option_id=$option->id;
                $challenge_option->option=$option->option_name;
                $challenge_option->point=$option->point;
                $challenge_option->status="Active";
                $challenge_option->save();
            }
}

    $challenge_start = new PlayerChallengeStart();
    $challenge_start->organization_id=$organization_id;
    $challenge_start->team_id=$team_id;
    $challenge_start->member_id=$user_id;
    $challenge_start->week_no=$week->id;
    $challenge_start->challenge_id=$request->challenge_id;
    $challenge_start->start_date=date("Y-m-d");
    $challenge_start->end_date=$week->enddate;
    $challenge_start->save();

     return response(['msg'=>"Challenge has been saved."], 200)
                  ->header('Content-Type', 'application/json');   
}

public function saveTodayChallenge(Request $request)
{
     $this->validate($request,[
        "challenge_id"=>"required",
    ]);
     $user_id = Auth::user()->id;
    $organization_id = Auth::user()->organization_id;
    $team_id = Team_player::where(['memebr_id'=>$user_id,'organization_id'=>$organization_id])->orderBy('id','DESC')->first()->team_id;
     $check = PlayerChallenge::where(["week_no"=>$request->week_no,"organization_id"=>$organization_id,"team_id"=>$team_id,"member_id"=>$user_id,"challenge_id"=>$request->challenge_id,"created_at"=>date('Y-m-d')])->count();
     if($check==0)
     {
    
    $oldchallenge = Challenge::find($request->challenge_id);
    $totalmark = PlayerChallenge::select(\DB::raw("sum(obtained_mark) as totalmark"))->where(["week_no"=>$request->week_no,"organization_id"=>$organization_id,"team_id"=>$team_id,"member_id"=>$user_id,"challenge_id"=>$request->challenge_id])->groupBy("challenge_id")->first()->totalmark;
    $challengs = new PlayerChallenge();
    $challengs->category_name=$oldchallenge->category->name;
    $challengs->csetid=$oldchallenge->setmap->set->id;
    $challengs->challenge_id=$request->challenge_id;
    $challengs->week_no=$request->week_no;
    $challengs->csettitle=$oldchallenge->setmap->set->title;
    $challengs->csetdescription=$oldchallenge->setmap->set->description;
    $challengs->challenge=$oldchallenge->challenge_name;
    $challengs->challenge_desc=$oldchallenge->challenge_description;
    $challengs->lang=$oldchallenge->lang_code;
    $challengs->organization_id=$organization_id;
    $challengs->team_id=$team_id;
    $challengs->member_id=$user_id;
    $challengs->complexity_of_challenge=$oldchallenge->complexity_of_challenge;
    $challengs->no_of_days_to_complete=$oldchallenge->no_of_days_to_complete;
    $challengs->obtained_mark=($request->mark-$totalmark);
    $challengs->selected_option=$request->option??0;
    $challengs->status=$oldchallenge->challenge_status;
    $challengs->save();
     return response(['msg'=>"Today Challenge has been completed."], 200)
                  ->header('Content-Type', 'application/json');  
}else{
   return response(['msg'=>"You already submitted the today challenge."], 200)
                  ->header('Content-Type', 'application/json');   
}

}

}
