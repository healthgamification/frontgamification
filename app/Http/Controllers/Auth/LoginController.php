<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use DB;
use Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
  //  protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm()
    {
        return view('auth.login');
    }


    public function login(Request $request)
    {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if (method_exists($this, 'hasTooManyLoginAttempts') &&
            $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

        return $this->sendLockoutResponse($request);
    }

    if ($this->guard()->validate($this->credentials($request))) {
        $user = $this->guard()->getLastAttempted();
        $error_message="You have not authorize to access this portal. Kindly contact to your administrator.";
        // Make sure the user is active
        if ($user->status=="Active") {
            $og = DB::table('organizations')->where(["id"=>$user->organization_id,"status"=>"Active"]);
            $team_player_status = DB::table('team_players')->where(["memebr_id"=>$user->id,"status"=>"Active"]);
            $team_status_count =$team_player_status->count();
            if($og->count()>0 && $team_status_count>0){
                $team_status = DB::table('teams')->where(["id"=>$team_player_status->first()->team_id,"status"=>"Active"])->count();
                if($team_status>0 && $this->attemptLogin($request))
                {
                    \Session::put('org_id',$og->first()->id);
            // Send the normal successful login response
                    return $this->sendLoginResponse($request);
                }
            }

        }
      // Increment the failed login attempts and redirect back to the
            // login form with an error message.
        $this->incrementLoginAttempts($request);
        return redirect()
        ->back()
        ->withInput($request->only($this->username(), 'remember'))
        ->with('msg',$error_message);

                // 
    }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
    $this->incrementLoginAttempts($request);
    return redirect()
    ->back()
    ->withInput($request->only($this->username(), 'remember'))
    ->with('msg',"These credentials do not match our records.");


}

}
