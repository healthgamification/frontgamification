<?php
if(!function_exists("time_elapsed_string"))
{
    function time_elapsed_string($datetime, $full = false) {
        $now = new DateTime;
        $ago = new DateTime($datetime);
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }

        if (!$full) $string = array_slice($string, 0, 1);
        return $string ? implode(', ', $string) . ' ago' : 'just now';
    }

}

if(!function_exists("translate"))
{
    function translate($item,$params=array())
    {
       $orgid = session('org_id');
       $org = \DB::table('organizations')->where(["status"=>"Active","id"=>$orgid])->first();
       $team_player_status = DB::table('team_players')->where(["memebr_id"=>\Auth::user()->id,"status"=>"Active"]);
       $team_status_count =$team_player_status->count();

       if($org && \Auth::user()->status=="Active" && $team_status_count>0)
       {
        $team_status = DB::table('teams')->where(["id"=>$team_player_status->first()->team_id,"status"=>"Active"])->count();
        if($team_status>0)
        {
           $lang_code = $org->lang_code;
           \Session::put("lang_code",$lang_code);
           $trans = App\Translation::where(["locale"=>$lang_code,"item"=>$item]);
           $transen = App\Translation::where(["locale"=>'en',"item"=>$item]);
           if($trans->count()>0 && $transen->count()>0)
           {
            $trans = $trans->first()->text;
            $transen = $transen->first()->text;
            if(empty($trans))
            {
                $trans=$transen;
            }
        }else{
            $trans='';
        }

        if(empty($trans))
        {
            $trans = $lang_code.'.'.$item;
        }
        preg_match_all("/:(\S+)/im", $trans, $matches);
        $amatches = array_map("mapfn",array_flip(str_replace(":","",$matches[0])));
        $values = array_merge($amatches,$params);
        $trans = str_replace($matches[0], array_values($values), $trans);
        return $trans;
    }else{
    //\Auth::logout();
   // return redirect("/logout");
        echo "<script> window.location.href='".route('logout')."'; </script>";
    }
}else{
    //\Auth::logout();
   // return redirect("/logout");
    echo "<script> window.location.href='".route('logout')."'; </script>";
}
}

function mapfn()
{
    return null;
}
}

if(!function_exists("setting"))
{
    function setting()
    {
     return (object) array(
        "show_result_click_on_complete_quizz"=>false
    );
 }
}

if(!function_exists("getDbData"))
{
    function getDbData($tbl,$value,$label)
    {
        $status=["status"=>"Active"];
        if($tbl=="users"){
            $status =["deleted_at"=>null];
        }
        return \DB::table($tbl)->select($label,$value)->orderBy($label,"asc")->where($status)->get()->pluck($value,$label)->all();
    }
}

if(!function_exists("send_mail")){
    function send_mail($to,$type,$data)
    {
        $template = App\MailTemplate::where(["status"=>"Active","type"=>$type])->first();
        if($template)
        {
            $body = makeText($template->body,$data);
            $subject = makeText($template->subject,$data);

            \Mail::send('emails.email', compact('body'), function ($message) use($to, $subject)
            {
                $message->to($to)->subject($subject);

            });
        }
    }



    function makeText($srt,$data){
        preg_match_all("/(\[\[\S+\]\])/im", $srt, $matches);
        $amatches = array_map("mapfn",array_flip(str_replace(["[[","]]"],["",""],$matches[0])));
        $values = array_merge($amatches,$data);
        return str_replace($matches[0], array_values($values), $srt);
    }

    function makemsg($type,$data)
    {
        $template = App\MailTemplate::where(["status"=>"Active","type"=>$type])->first();
        if($template)
        {
            $body = makeText($template->body,$data);
            $subject = makeText($template->subject,$data);

            return ["subject"=>$subject,"body"=>$body];
        }
    }
}