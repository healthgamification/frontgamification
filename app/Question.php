<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class Question extends Model
{
    //
    public function category()
    {
    	return $this->belongsto(QuestionsCategory::class);
    }

    public function categories()
    {
    	return $this->hasMany(QuestionCategoryMapping::class,'question_id','id')->where('status','Active');
    }

    public function selected()
    {
    	return $this->hasMany(QuestionSetMapping::class,'question_id','id');
    }

    public function answers()
    {
        return $this->hasMany(Answers::class,'question_id','id')->where('status','Active');
    }
}
