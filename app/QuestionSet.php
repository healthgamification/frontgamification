<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class QuestionSet extends Model
{
    //
     protected $guarded = ['id'];

     public function questions()
     {
     	return $this->hasMany(QuestionSetMapping::class,'question_set_id','id')->where('status','Active')->orderByRaw("rand()");
     }


     public function played()
     {
          $user_id = \Auth::user()->id;
    $organization_id = \Auth::user()->organization_id;
    $team_id = Team_player::where(['memebr_id'=>$user_id,'organization_id'=>$organization_id])->orderBy('id','DESC')->first()->team_id;
     $date=date("Y-m-d");
        $week = AssignWeekModel::where("status","Active")
        ->where("startdate","<=",$date)
        ->where("enddate",">=",$date)
        ->where("org_id",$organization_id)
        ->first();
        if(!$week)
        {
             $week = AssignWeekModel::where("status","Active")
             ->where("startdate",">",$date)
        ->where("org_id",$organization_id)->orderBy("id","asc")
        ->first();
        }
        $week_no = $week->id;
          return $this->hasMany(PlayerQuestion::class,'qsetid','id')->where(["organization_id"=>$organization_id,"memebr_id"=>$user_id,"team_id"=>$team_id,"week_no"=>$week_no]);
     }
}
