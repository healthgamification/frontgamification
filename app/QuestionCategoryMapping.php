<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionCategoryMapping extends Model
{
    //
    public function category()
    {
        return $this->belongsTo(QuestionsCategory::class,'question_cat_id','id');
    }

    public function question()
    {
        return $this->belongsTo(Question::class,'question_id','id');
    }
}
