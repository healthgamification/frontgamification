<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class Challenge extends Model
{
	

    protected $guarded = ['id'];
    public function category()
    {
    	return $this->hasOne(ChallengeCategory::class,'id','challenge_category_id');
    }

    public function setmap()
    {
        return $this->hasOne(ChallengeSetMappings::class,'challenge_id','id');
    }

    public function selected()
    {
    	return $this->hasMany(ChallengeSetMappings::class,'challenge_id','id');
    }

    public function options()
    {
        return $this->hasMany(ChallengeOption::class,'challenge_id','id');
    }


     public function played()
     {
        $user_id = \Auth::user()->id;
    $organization_id = \Auth::user()->organization_id;
    $team_id = Team_player::where(['memebr_id'=>$user_id,'organization_id'=>$organization_id])->orderBy('id','DESC')->first()->team_id;
     $date=date("Y-m-d");
   $week = AssignWeekModel::where("status","Active")
        ->where("startdate","<=",$date)
        ->where("enddate",">=",$date)
        ->where("org_id",$organization_id)
        ->first();
        if(!$week)
        {
             $week = AssignWeekModel::where("status","Active")
             ->where("startdate",">",$date)
        ->where("org_id",$organization_id)->orderBy("id","asc")
        ->first();
        }
        $week_no = $week->id;
          return $this->hasOne(PlayerChallengeStart::class,'challenge_id','id')->where(["organization_id"=>$organization_id,"member_id"=>$user_id,"team_id"=>$team_id,"week_no"=>$week_no]);
     }

     public function totalmark($week_no)
     {
         $user_id = \Auth::user()->id;
    $organization_id = \Auth::user()->organization_id;
    $team_id = Team_player::where(['memebr_id'=>$user_id,'organization_id'=>$organization_id])->orderBy('id','DESC')->first()->team_id;
        return PlayerChallenge::select(\DB::raw("sum(obtained_mark) as totalmark"))->where(["week_no"=>$week_no,"organization_id"=>$organization_id,"team_id"=>$team_id,"member_id"=>$user_id,"challenge_id"=>$this->id])->groupBy("challenge_id")->first()->totalmark;
     }





}
