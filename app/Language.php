<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class Language extends Model
{
    //
     protected $guarded = ['id'];

    public function translations()
    {
    	return $this->hasMany(Translation::class,'locale','locale')->orderBy("item",'asc');
    }
}
