<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionsCategory extends Model
{
	
    protected $table = 'questions_category';

     public function questions()
    {
    	return $this->hasMany(QuestionCategoryMapping::class,'question_cat_id','id');
    }
}
