<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionSetMapping extends Model
{
    //
    protected $guarded = ['id'];
    
     public function question()
     {
     	return $this->hasOne(Question::class,'id','question_id')->where('status','Active');
     }

}
