<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    //

    protected $appends = ['point','rank'];

    public function getPointAttribute()
    {
        $point = PlayerPoint::select(\DB::raw("sum(member_point) as point"))->groupBy("team_id")->where("team_id",$this->id);
		$points=0;
		if($point->count()>0)
		{
			$points = $point->first()->point;
		}

		return round($points);
    }

    public function getRankAttribute()
    {
        $orgid = Team::find($this->id);
        $rank = \DB::select("SELECT team_id, FIND_IN_SET( sum(member_point), (SELECT GROUP_CONCAT(member_point_sum ) FROM (SELECT team_id,sum(member_point) as member_point_sum from player_points where organization_id=$this->organization_id group by team_id order by member_point_sum desc) a) ) AS rank FROM player_points where organization_id=$this->organization_id and team_id=$this->id group by team_id");
        $mrank=0;
        if(count($rank)>0)
        {
            $mrank = $rank[0]->rank;
        }

        return $mrank;
    }

    public function players()
    {
    	return $this->hasMany(Team_player::class);
    }

    public function player_by_point_asc()
    {
        return Team_player::where(["organization_id"=>$this->organization_id,"team_id"=>$this->id,"status"=>"Active"])->get()->sortBy(function($team_player){
            return -$team_player->member->my_point;
        });
    }


}
