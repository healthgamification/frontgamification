<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class DynamicField extends Model
{
    protected $guarded = ['id'];

    protected $table="dynamic_fields";

    public function user()
    {
    	return $this->hasOne(EmployeeDetails::class,'field_id','id')->where("member_id",\Auth::id());
    }
}