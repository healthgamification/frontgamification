<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class PlayerChallengeStart extends Model
{
    //
     protected $guarded = ['id'];

     protected $table="player_challenge_start";

     public function started_challenge()
     {
     	return $this->hasOne(PlayerChallenge::class,'challenge_id','challenge_id')->orderBy("id",'desc');
     }

}
