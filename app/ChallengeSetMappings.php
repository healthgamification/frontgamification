<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChallengeSetMappings extends Model
{
    //
    protected $guarded = ['id'];

    public function challenges()
    {
    	return $this->hasOne(Challenge::class,'id','challenge_id');
    }

    public function set()
    {
        return $this->hasOne(ChallengeSets::class,'id','challenge_set_id');
    }
}
