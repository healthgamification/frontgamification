<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

use App\Notifications\PasswordReset;

class PlayerModel extends Authenticatable
{
	use Notifiable;
    //
    protected $table ="members";

    protected $appends = ['my_point','my_team_rank','my_team_point'];

    public function getMyPointAttribute()
    {
        $point = PlayerPoint::select(\DB::raw("sum(member_point) as member_point"))->groupBy("memebr_id")->where("memebr_id",$this->id);
        $points=0;
        if($point->count()>0)
        {
           $points = $point->first()->member_point;
       }

       return round($points);
   }

   public function getMyTeamPointAttribute()
   {
     $team_id = Team_player::where("memebr_id",$this->id)->orderBy('id','DESC')->first()->team_id;
     $point = PlayerPoint::select(\DB::raw("sum(member_point) as team_point"))->where("team_id",$team_id);
     $points=0;
     if($point->count()>0)
     {
       $points = $point->first()->team_point;
   }

   return round($points);
}

public function getMyTeamRankAttribute()
{
 $team_id = Team_player::where("memebr_id",$this->id)->orderBy('id','DESC')->first()->team_id;
 $rank = Team::find($team_id);
 return $rank->rank;
}

public function my_point_current_team($team_id)
{
    $point = PlayerPoint::select(\DB::raw("sum(member_point) as member_point"))->groupBy("memebr_id")->where("memebr_id",$this->id)->where("team_id",$team_id);
    $points=0;
    if($point->count()>0)
    {
        $points = $point->first()->member_point;
    }

    return round($points);
}

public function quizzes()
{
    $user_id =$this->id;
    $organization_id = $this->organization_id;
    $team_id = Team_player::where(['memebr_id'=>$user_id,'organization_id'=>$organization_id])->orderBy('id','DESC')->first()->team_id;
    $date=date("Y-m-d");
    $week = AssignWeekModel::where("status","Active")
        ->where("startdate","<=",$date)
        ->where("enddate",">=",$date)
        ->where("org_id",$organization_id)
        ->first();
        if(!$week)
        {
             $week = AssignWeekModel::where("status","Active")
             ->where("startdate",">",$date)
        ->where("org_id",$organization_id)->orderBy("id","asc")
        ->first();
        }
        $week_no = $week?$week->id:0;
    return $this->hasMany(PlayerQuestion::class,'memebr_id','id')->where(["organization_id"=>$organization_id,"team_id"=>$team_id,"week_no"=>$week_no])->groupBy("qsetid");
}

public function challenges()
{
    $user_id =$this->id;
    $organization_id = $this->organization_id;
    $team_id = Team_player::where(['memebr_id'=>$user_id,'organization_id'=>$organization_id])->orderBy('id','DESC')->first()->team_id;
    $date=date("Y-m-d");
    $week = AssignWeekModel::where("status","Active")
        ->where("startdate","<=",$date)
        ->where("enddate",">=",$date)
        ->where("org_id",$organization_id)
        ->first();
        if(!$week)
        {
             $week = AssignWeekModel::where("status","Active")
             ->where("startdate",">",$date)
        ->where("org_id",$organization_id)->orderBy("id","asc")
        ->first();
        }
        $week_no = $week?$week->id:0;
    return $this->hasMany(PlayerChallenge::class,'member_id','id')->where(["organization_id"=>$organization_id,"team_id"=>$team_id,"week_no"=>$week_no])->groupBy("challenge_id");
}

 /**
 * Send the password reset notification.
 *
 * @param  string  $token
 * @return void
 */
public function sendPasswordResetNotification($token)
{
    $this->notify(new PasswordReset($token));
}

}
