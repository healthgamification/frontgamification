<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class PlayerChallenge extends Model
{
    //
     protected $guarded = ['id'];

     protected $table="player_challenges";

     public function option()
     {
     	return $this->hasOne(PlayerChallengeOption::class,"option_id","selected_option")->select("option_id as id","option as name","point");
     }

}
