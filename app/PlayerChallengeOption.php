<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class PlayerChallengeOption extends Model
{
    //
     protected $guarded = ['id'];

     protected $table="player_challenge_options";

}
