<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class ChallengeSets extends Model
{
    //
     protected $guarded = ['id'];

     public function challengemapping()
     {
     	return $this->hasMany(ChallengeSetMappings::class,'challenge_set_id','id')->where('status', 'Active');
     }

    public function selected($orgid)
     {
     	return $this->hasMany(AssignChallengeWeekModel::class,'challenge_set_id','id')->orderBy("week_id",'asc')->where("org_week_challenge_assigns.org_id",$orgid)->get();
     }
}
