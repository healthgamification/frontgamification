<?php

namespace App\Providers;

use Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;

class MailConfigServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        if (\Schema::hasTable('mail_configs')) {
            $settings = DB::table('mail_configs')->orderBy("updated_at","desc")->first();
                $from=array();
                if($settings)
                {
                $from = json_decode($settings->from,true);
                }
                $config = array(
                   'driver' => $settings->driver??'smtp',
                'host' => $settings->host ?? 'smtp.mailtrap.io',
                'port' => $settings->port ?? 2525,
                'from' => [
                    'address' => isset($from[0])?$from[0]:'game@game.com',
                    'name' => isset($from[1])?$from[1]:env('APP_NAME'),
                ],
                'encryption' => $settings->encryption??'tls',
                'username' => $settings->username ?? '3783ff5b6b89ba',
                'password' => $settings->password ?? 'ce250835f4456b',
                'sendmail' => $settings->sendmail ?? '/usr/sbin/sendmail -bs',
                'pretend' => $settings->pretend??false,
                );
                Config::set('mail', $config);
        }
    }
}