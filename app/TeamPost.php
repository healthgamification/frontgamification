<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TeamPost extends Model
{
    //

    public function member()
    {
    	return $this->hasOne(PlayerModel::class,'id',"member_id");
    }
}
