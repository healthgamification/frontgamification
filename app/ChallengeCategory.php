<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChallengeCategory extends Model
{
    //
    public function challenges()
    {
    	return $this->hasMany(Challenge::class,'challenge_category_id','id');
    }
}
