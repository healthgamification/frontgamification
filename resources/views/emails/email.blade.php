<!DOCTYPE html>
<html>
<head>
	<title>Mailer</title>
</head>
<body>

	<table align="center" border="1" style="border-radius:4px; border:1px solid #cccccc; font-family:sans-serif; padding:20px 35px 35px; text-align:center; width:500px">
		<tbody>
			<tr>
				<td>
					<h1>Health Gamification System</h1>
				</td>
			</tr>
			<tr>
				<td>
					{!! $body !!}   
				</td>
			</tr>  
		</tbody>
	</table>

</body>
</html>