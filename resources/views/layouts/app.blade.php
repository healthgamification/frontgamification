<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <link href='http://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/owl.carousel.min.css">
<link rel="stylesheet" href="css/jquery.range.css">
<link rel="shortcut icon" href="images/favicon.ico">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<noscript>Put Sample code here for execution when JavaScript is Disabled</noscript>
</head>
<body class="my-profile-bg">
<div class="container">
<div id="menuCloseButton"></div>

    <!-- header start-->
    <header id="header" class="linkclick">
        <nav>
            <ul>
                <li><a href="#dashboard" data-content="{{route('dashboard',app()->getLocale())}}" data-bclass="my-profile-bg" >{{translate('MY_PROFILE')}}</a></li>
                <li><a href="#mygroup" data-content="{{route('mygroup',app()->getLocale())}}" data-bclass="my-group-bg">{{translate('MY_GROUP')}}</a></li>
                <li><a href="#group" data-content="{{route('group',app()->getLocale())}}" data-bclass="group">{{translate('GROUP')}}</a></li>
                <li><a href="#quizzes" data-content="{{route('quiz',app()->getLocale())}}" data-bclass="quizess-bg">{{translate('QUIZZ_PLURAL')}}</a></li>
                <li><a href="#challenge" data-content="{{route('challenge',app()->getLocale())}}" data-bclass="hacks-bg">{{translate('CHALLENGE_PLURAL')}}</a></li>
                <li><a href="#pointtable" data-content="{{route('pointtable',app()->getLocale())}}" data-bclass="point-table-bg">{{translate('POINT_TABLE')}}</a></li>
                <li><a href="#info" data-content="{{route('info',app()->getLocale())}}" data-bclass="info-bg">{{translate('INFO')}}</a></li>
                <li><a href="{{route('logout')}}">{{translate('LOGOUT')}}</a></li>  
                <li style="display: none;">
                    <a href="#edit-profile" style="display: none;" data-content="{{route('edit',app()->getLocale())}}" data-bclass="edit-profile" id="edit-profile">Edit profile</a>
                </li>  
            </ul>
        </nav>
        
    </header> 
            @yield('content')
    </div>
    <div class="flying-girl">
    <img src="images/girl-with-baloon.png" alt="" />
    
</div>
<script  src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="js/jquery.range-min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="js/custom.js"></script>
@yield('js')
</body>
</html>
