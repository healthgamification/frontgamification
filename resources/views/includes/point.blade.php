	<div class="col-md-3 col-md-pull-9 slideInLeft">
			<div class="main-left linkclick">
				<div class="avtar">
					<img id="useravatar" src="images/large-avtar/{{Auth::user()->avatar}}" alt="" />
				</div>
				<div class="profile-info">
					<h1 id="profilenameview">{{Auth::user()->member_name}}</h1>
					<ul>
						<li><span class="point-number">{{Auth::user()->my_point}}</span>  <span class="point-text">{{translate('POINTS')}}</span></li>
						<li><span class="point-number">#{{Auth::user()->my_team_rank}}</span>  <span class="point-text">{{translate('TEAM_RANK')}}</span></li>
						<li><span class="point-number">{{Auth::user()->my_team_point}}</span>  <span class="point-text">{{translate('TEAM_POINTS')}}</span></li>
					</ul>
				</div>
				<a href="#edit-profile" data-content="{{route('edit',app()->getLocale())}}" data-bclass="edit-profile" id="edit-profile">{{translate('EDIT_PROFILE_BTN_TXT')}}</a>
			</div>
		</div>
		<div class="clearfix"></div>