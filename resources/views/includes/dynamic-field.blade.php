@forelse($profile_info as $field)
@php 
$opdata = $field->options ?? '[]';
$options = json_decode($opdata);
@endphp
<div class="col-md-6 form-group">	
	<label {{$field->attr}}>{{$field->label}}</label>
	@if($field->type =='text')
	<input type="text" class="form-control" data-validation="required" name="profile_info[{{$field->name}}]" value="{{old($field->name,($field->user->value??''))}}" placeholder="Enter {{$field->label}}" {{$field->attr}} />	
	@elseif($field->type =='select' && !empty($options))
	<select class="form-control" name="profile_info[{{$field->name}}]" style="padding: 7px 25px; height: 43px;" {{$field->attr}}>
		@foreach($options->value as $key=>$val)
		<option value="{{$val}}" {{($field->user->value??'')==$val?'selected':''}}>{{$options->label[$key]}}</option>
		@endforeach
	</select>
	@elseif($field->type =='dbselect' && !empty($options))
	<select class="form-control" name="profile_info[{{$field->name}}]" style="padding: 7px 25px; height: 43px;" {{$field->attr}}>
	@foreach(getDbData($options->tblname[0],$options->label[0],$options->value[0]) as $key=>$val)
	<option value="{{$key}}" {{($field->user->value??'')==$val?'selected':''}} >{{$val}}</option>
	@endforeach
	</select>
	@elseif($field->type =='radio' && !empty($options))
	<br>
	@foreach($options->value as $key=>$val)
		<input type="radio" {{($field->user->value??'')==$val?'checked':''}} name="profile_info[{{$field->name}}]"  value="{{$val}}" {{$field->attr}}>
	<span>{{$options->label[$key]}}</span>
	@endforeach
	@elseif($field->type =='checkbox' && !empty($options))
	<br>
	@foreach($options->value as $key=>$val)
	@php $v = json_decode(($field->user->value??'[]'),true); @endphp
		<input type="checkbox" {{in_array($val,$v)?'checked':''}} name="profile_info[{{$field->name}}][]" value="{{$val}}" {{$field->attr}}>
	<span>{{$options->label[$key]}}</span>
	@endforeach
	@elseif($field->type =='textarea')
	<textarea class="form-control" name="profile_info[{{$field->name}}]" {{$field->attr}}>{{old($field->name,($field->user->value??''))}}</textarea>
	@elseif($field->type =='datepicker')
	<input type="text" class="form-control datepicker" {{$field->attr}} name="profile_info[{{$field->name}}]" value="{{old($field->name,($field->user->value??''))}}" placeholder="Enter {{$field->label}}" />	
	@endif
	<div class="form-error" id="{{$field->name}}"></div>
</div>
@empty
@endforelse