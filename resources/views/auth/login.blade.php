@extends('layouts.login')

@section('content')
    <div class="container" id="app">

    <!-- login start -->
    <div class="login-wrapper">
        <div class="login-logo">
            <img src="images/login-logo.png" alt=""/>
        </div>
        <div class="login-from-outer">

            <form action="{{ route('login') }}" id="loginForm" method="POST"> 
            @csrf            
            <p id="errormsg"><strong>{{Session::has('msg')?Session::get('msg'):''}}</strong></p>
            <!-- login box start -->
            <div class="login-box slideInLeft">
                <div class="col-md-12 form-group @if ($errors->has('member_email')) has-error @endif">
                    <label>@lang(Lang::locale().'.EMAIL')</label>
                    <input type="text" name="member_email" value="{{old('member_email')}}" class="form-control" autocomplete="off" data-validation="email" data-validation="required" data-validation-error-msg="You have not provide a correct e-mail address"/>
                     @if ($errors->has('member_email'))
                        <span class="help-block form-error">
                            <strong>{{ $errors->first('member_email') }}</strong>
                        </span>
                        @endif
                </div>              
                <div class="col-md-12 form-group @if ($errors->has('password')) has-error @endif">
                    <label>@lang(Lang::locale().'.PASSWORD')</label>
                    <input type="password" name="password" class="form-control" autocomplete="off" data-validation="required"/>
                     @if ($errors->has('password'))
                        <span class="help-block form-error" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                        @endif
                </div>
                <div class="col-md-12">
                    <div class="pull-right mb-15">
                        <a class="link-login-form" id="forgot_password">@lang(Lang::locale().'.FORGOT') @lang(Lang::locale().'.PASSWORD') ?</a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-12">
                    <div class="button-box">
                    <button type="submit" name="submit" class="btn-login-form">@lang(Lang::locale().'.LOGIN')</button>
                    <!-- <a href="#"  class="btn-login-form">Login</a> -->
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            </form>
            <!-- login box end -->

            <!-- forgot box start -->
            <div class="forgot-box slideInLeft" style="display: none;">
             <form id="forgotForm" onsubmit="return resetpassword(this);" action="{{ route('password.email') }}" method="POST" autocomplete="off">  
                 @csrf
                <div class="col-md-12">
                    <label>@lang(Lang::locale().'.EMAIL')</label>
                    <input type="text" class="form-control" data-validation="email" data-validation="required" data-validation-error-msg="You have not provide a correct e-mail address" name="member_email" />
                        <span class="help-block form-error" id="member_email"></span>
                </div>
                    <div class="alert alert-success mt-4" id="successmsg" role="alert" style="display: none;"> 
                    </div>
               
                <div class="col-md-12">
                    <div class="pull-right mb-15">
                        <a href="#;" class="link-login-form" id="back_login">Back to login</a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-12">
                    <div class="button-box">
                    <button type="submit" name="forgotPassword" class="btn-login-form">Send</button>
                    <!-- <a href="#;" class="btn-login-form" id="send_mail">Send</a> -->
                    </div>
                </div>
                <div class="clearfix"></div>
                </form>
            </div>
            <!-- forgot box end -->
        </div>

        <!-- information start -->
        <div class="login-info-wraper">
            <div class="main-info-title">
                <h1>@lang(Lang::locale().'.HOW_TO_PLAY')</h1>
            </div>
            <p>@lang(Lang::locale().'.HOW_TO_PLAY_DESCRIPTION')</p>
        </div>

        <div class="info-wrapper">
            <div class="info-outer-box">
                <div class="info-title">
                    <h1>@lang(Lang::locale().'.TEAM_PLURAL')</h1>
                </div>
                <p><span class="open-quote"></span>
                    @lang(Lang::locale().'.TEAM_DESCRIPTION_TXT')
                    <span class="close-quote"></span></p>
            </div>
        </div>

        <div class="info-wrapper">
            <div class="info-outer-box">
                <div class="info-title">
                    <h1>@lang(Lang::locale().'.CHALLENGE_PLURAL')</h1>
                </div>
                <p><span class="open-quote"></span>
                     @lang(Lang::locale().'.CHALLENGE_DESCRIPTION_TXT')
                    <span class="close-quote"></span></p>
            </div>
        </div>

        <div class="info-wrapper">
            <div class="info-outer-box">
                <div class="info-title">
                    <h1>@lang(Lang::locale().'.QUIZZ_PLURAL')</h1>
                </div>
                <p><span class="open-quote"></span>
                     @lang(Lang::locale().'.QUIZZ_DESCRIPTION_TXT')
                    <span class="close-quote"></span></p>
            </div>
        </div>

        

        <!-- information start -->
    </div>

</div>

<div class="flying-girl">
    <img src="images/girl-with-baloon.png" alt="" />
</div>
@endsection
