<div class="col-md-9 col-md-push-3 slideInLeft">
			<div class="main-right">

				<!-- point table start -->
				<div class="point-table-wrapper panel-group" id="accordion">
					@php
					$rank=1;
					@endphp
					@forelse($teams as $key=>$team)
					<div class="accordion-outer panel">
						<div class="accordion-header" data-toggle="collapse" data-parent="#accordion" data-target="#collapse{{$key}}">
							<div class="header-item-left">
								<span class="head-number pull-left">#{{$rank}}</span>
								<span class="head-name pull-left">{{$team->name}}</span>
								<span class="clearfix"></span>
							</div>
							<div class="header-item-right">
								<span class="head-count">{{$team->point}}</span>
								<span class="head-point">{{translate('POINTS')}}</span>
								<button class="head-arrow"><span class="arrow-icon rotate">&gt;</span></button>
							</div>
							<div class="clearfix"></div>
						</div>
						<div id="collapse{{$key}}" class="accordion-body collapse {{$team->id==$team_id?'in':''}}">
							<p class="accordion-info">{{$team->description}}</p>
							<div class="user-wrap-outer">
								<div class="row">
									@forelse($team->player_by_point_asc() as $player)
									<div class="col-md-2 col-sm-4 col-xs-6">
										<div class="user-box">
											<div class="user-image">
												<img src="images/{{$player->member->avatar}}" alt="user"/>
											</div>
											<p class="username-text">{{$player->member->member_name}}</p>
											<span class="user-point">{{$player->member->my_point_current_team($team->id)}}</span>
											 
										</div>
									</div>
									@empty
									@endforelse
									<div class="clearfix"></div>
								</div>
							</div>
						</div>
					</div>
					@php
					$rank++;
					@endphp
					@empty
					@endforelse
				</div>
				<!-- point table end -->
				
			</div>
		</div>

  @include('includes.point')