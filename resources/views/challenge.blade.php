<div class="col-md-9 col-md-push-3 slideInLeft">
			<div class="main-right">

				<!-- hacks start -->
				<div class="quizzes-wrapper">
					<div class="activity-inner-wrap">
						<div class="row">
							
							@forelse($challengs as $challenge)
							@if(!$challenge->played)
							@if($totalplayed < Auth::user()->no_of_challenge)
							<div class="col-sm-2 col-xs-4">
								<div class="circle-box" onclick="showchallengestartmodal(this)" data-challenge_name="{{$challenge->challenge_name}}" data-challenge_description="{{$challenge->challenge_description}}" data-challenge_id="{{$challenge->id}}" data-challenge_options="{{$challenge->options->pluck('option_name','id')}}">
									<img src="images/challange_active.png" alt="">
									<p>{{$challenge->challenge_name}}</p>
								</div>
							</div>
							@else
							<div class="col-sm-2 col-xs-4">
								<div class="circle-box" data-toggle="modal" data-target=".rich-weekly-modal">
									<img src="images/challange_active.png" alt="">
									<p>{{$challenge->challenge_name}}</p>
								</div>
							</div>
							@endif
							@else
							@php
							$date1=date_create(date('Y-m-d'));
							$date2=date_create($challenge->played->start_date);
							$diff=date_diff($date1,$date2);
							$day = $diff->d+1;
							$totalpoint =100;
							if(!empty($challenge->played->started_challenge->option) && $challenge->played->started_challenge->option->point !=0)
							{
								$totalpoint=$challenge->played->started_challenge->option->point;
							}
							$perday=($totalpoint/$challenge->no_of_days_to_complete);
							@endphp
							<div class="col-sm-2 col-xs-4">
								<div class="circle-box" onclick="showdailychallengemodal(this);" data-challenge_name="{{$challenge->challenge_name}}" data-challenge_description="{{$challenge->challenge_description}}" data-challenge_id="{{$challenge->id}}" data-challenge_options="{{$challenge->played->started_challenge->option}}" data-mark="{{$challenge->totalmark($week_no)}}" data-day="{{$day}}" data-perday="{{$perday}}" data-todaydate="{{strtotime(date('Y-m-d',strtotime($challenge->played->started_challenge->created_at)))}}" data-currentday="{{strtotime(date('y-m-d'))}}" data-choose_msg="{{translate('challenge_option_choose_txt')}}">
									<img src="images/challange_done.png" alt="">
									<p>{{$challenge->challenge_name}}</p>
								</div>
							</div>
							@endif
							@empty
							<div class="col-md-12">
								@if($week)
								<p style="text-align: center;  font-size: 42px;  margin-top: 0px;" id="upcommingevent"></p>
								<script type="text/javascript">countdown("{{$week->startdate}}","upcommingevent");</script>
								@else
								<p style="text-align: center;  font-size: 42px;  margin-top: 0px;">{{translate("not_game_found_msg")}}</p>
								@endif
							</div>
							@endforelse							
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
				<!-- hacks end -->

			</div>
		</div>

  @include('includes.point')

  <!-- modal one hacks start -->
<div class="modal fade smaller-plate-modal" id="challenge_start_modal" role="dialog" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<a href="javascript:void(0);" class="btn-close" data-dismiss="modal">
					<img src="images/delete-orange.png" alt=""/>
				</a>
				<h1 class="challenge-title"></h1>
				<form method="post" id="challenge_start_form">
					@csrf
					<input type="hidden" name="challenge_id" id="challenge_id">
					<input type="hidden" name="week_no" id="week_no" value="{{$week_no}}">
				<p class="challenge-msg"></p>

				<div class="question-box" id="challeng_options">
				</div>	
				<p class="challenge_option_error_msg"></p>			
				</form>
				<button class="btn-form" onclick="startchallenge()">{{translate('start_challenge_btn')}}</button>
				<button class="btn-form" data-dismiss="modal">{{translate('close_btn')}}</button>
			</div>
		</div>
	</div>
</div>
<!-- modal one hacks end -->

<!-- modal two hacks start -->
<div class="modal fade race-modal" role="dialog" id="challenge_after_start_modal" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<a href="javascript:void(0);" class="btn-close" data-dismiss="modal">
					<img src="images/delete-orange.png" alt=""/>
				</a>
				<h1 class="challenge_after_title"></h1>
				<p class="challenge_after_msg"></p>
				
				<form method="post" id="challenge_after_start_form">
				<div id="after_start_msg"></div>
				<h4>{!! translate('challenge_running_modal_txt') !!}
				</h4>
					@csrf
					<input type="hidden" name="challenge_id" id="afterchallenge_id">
					<input type="hidden" name="week_no"  value="{{$week_no}}">
				<!-- range slider start -->
				<input class="simple-slider" name="mark"  type="hidden" value="0"/>
				<!-- range slider start -->
			</form>

				<button class="btn-form" id="savetodaychallengebtn" onclick="savetodaychallenge();" >{{translate('save_exit_btn')}}</button>
				<button class="btn-form" data-dismiss="modal">{{translate('cancel_btn')}}</button>
			</div>
		</div>
	</div>
</div>
<!-- modal two hacks end -->

<!-- modal quizzes start -->
<div class="modal fade rich-weekly-modal smaller-plate-modal" role="dialog" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<a href="javascript:void(0);" class="btn-close modal-close"  data-dismiss="modal">
					<img src="images/delete-blue.png" alt=""/>
				</a>
				
				 <h1>{{translate('challenge_limit_reach_modal_title')}}</h1>
				<p>{!! translate('challenge_limit_reach_modal_msg',["totalplayed"=>$totalplayed]) !!}</p>
				<button class="btn-form modal-close" data-dismiss="modal">{{translate('cancel_btn')}}</button>
			</div>
		</div>
	</div>
</div>
<!-- modal quizzes end -->

<script type="text/javascript">
	$(document).ready(function(){
 $('.simple-slider').jRange({
    from: 0,
    to: 100,
    step: 1,
    scale: [5,25,50,75,100],
    format: '%s',
    width: 400,
    showLabels: true,
    isRange:false,
    slide: function(event, ui) {
      $projectPercent.val(ui.value + "%");
    },
    onstatechange:function(value){
    	if(value==80)
    	{
    		this.options.isDrag=false;
    	}
    }
  });
 
});

</script>
