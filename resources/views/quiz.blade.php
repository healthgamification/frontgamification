<div class="col-md-9 col-md-push-3 slideInLeft">
			<div class="main-right">

				<!-- quizzes start -->
				<div class="quizzes-wrapper">
					<div class="activity-inner-wrap">
						<div class="row">
							<!-- <div class="col-sm-2 col-xs-4">
								<div class="circle-box">
									<img src="images/quiz_done.png" alt="">
									<p>Your body & joy</p>
								</div>
							</div> -->
							@forelse($quizzes as $quiz)
							@if($quiz->quizzes->played->count()==0)
							@if($totalplayed < Auth::user()->no_of_quizzes)
							<div class="col-sm-2 col-xs-4">
								<div class="circle-box" onclick="showquizmodal(this);" data-quiz_name="{{$quiz->quizzes->title}}" data-quiz_msg="{{translate('START_QUIZZ_MSG_TXT',['question_coint'=>$quiz->quizzes->questions->count(),'second'=>30])}}" data-quizz_id="{{$quiz->quizzes->id}}" data-weekno="{{$quiz->week_id}}" data-cancel_modal_title="{{translate('are_you_sure')}}" data-cancel_modal_msg="{{translate('cancle_modal_msg')}}">
									<img src="images/quiz_thumb.png" alt="">
									<p>{{$quiz->quizzes->title}}</p>
								</div>
							</div>
							@else
							<div class="col-sm-2 col-xs-4">
								<div class="circle-box" data-toggle="modal" data-target=".rich-weekly-modal">
									<img src="images/quiz_thumb.png" alt="">
									<p>{{$quiz->quizzes->title}}</p>
								</div>
							</div>
							@endif
							@else
							<div class="col-sm-2 col-xs-4">
								@if(setting()->show_result_click_on_complete_quizz)
								<div class="circle-box" onclick="showquizzresult(this)" data-quiz_name="{{$quiz->quizzes->title}}" data-quizz_id="{{$quiz->quizzes->id}}" data-weekno="{{$quiz->week_id}}">
									@else
									<div class="circle-box">
									@endif
									<img src="images/quiz_done.png" alt="">
									<p>{{$quiz->quizzes->title}}</p>
								</div>
							</div>
							@endif
							@empty
							<div class="col-md-12">
								@if($week)
								<p style="text-align: center;  font-size: 42px;  margin-top: 0px;" id="upcommingevent"></p>
								<script type="text/javascript">countdown("{{$week->startdate}}","upcommingevent");</script>
								@else
								<p style="text-align: center;  font-size: 42px;  margin-top: 0px;">{{translate("not_game_found_msg")}}</p>
								@endif
							</div>
							@endforelse
						</div>
					</div>
				</div>
				<!-- quizzes end -->

			</div>
		</div>

  @include('includes.point')

  <!-- modal quizzes start -->
<div class="modal fade quizzes-modal" id="quizz_start_modal" role="dialog" data-backdrop="static">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-body">
				<a href="javascript:void(0)" class="btn-close modal-close"  data-dismiss="modal">
					<img src="images/delete-blue.png" alt=""/>
				</a>
				
				<!-- <h1>Unfortunately...</h1> -->
				<h1 class="quizz-title"></h1>
<style type="text/css">
	.answer{
		margin-left: 40px;
	}
	.answer li {
		list-style: upper-alpha;
	}
	.answer li label {
		margin-left: 5px;
		font-size: 15px;
		font-weight: 400;
	}
	.correctans{
		text-align: center;
    font-size: 24px;
    padding: 10px;
    border: 2px solid #fff;
    background: #fff;
    color: #000;
    border-radius: 5px;
	}
</style>
				<!-- <p>You have already taken 3 quizzes this week, <br/>
				but be patient - next week you can take new quizzes again</p> -->

				<div id="before_start">
				<p class="quizz-description-txt"></p>
				<form id="start_form" autocomplete="off" method="post">
					@csrf
					<input type="hidden" name="quizz_id" id="quizz_id" value="">
				</form>

				<!-- <button class="btn-form" data-dismiss="modal">OK</button> -->
				<button class="btn-form" onclick="start_quizz();">{{translate('start_quizz_btn')}}</button>
				<button class="btn-form modal-close" data-dismiss="modal">{{translate('cancel_btn')}}</button>
			</div>
			<div id="after_start" style="display: none;">
				<div class="col-md-12 text-right">
					{{translate('quizz_start_timer_txt')}} : <span id="timer">30</span>
				</div>
				<div class="col-md-12">
					<div class="quiz-progress-bar">
					<div class="progress">
                        <div id="timerprogressbar" class="progress-bar pink progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:0%"></div>
					</div>
					</div>
				</div>
				<form id="afterstart_form" autocomplete="off" method="post">
					@csrf
				<div class="col-md-12 text-left">
					<ul id="maincontenair"></ul>
				</div>
			</form>
				<div class="col-md-12 text-right" id="buttons">
					<!-- <button type="button" class="btn-form">Privious</button> -->
					<button style="display: none;" onclick="final_quizz_submit();" type="button" class="btn-form">{{translate('submit_btn')}}</button>
					<button type="button" id="quizznextbtn" onclick="nextquestion(this)" class="btn-form">{{translate('next_btn')}}</button>
				</div>
			</div>
			<div id="quizz_result">
				
			</div>
			<div class="clearfix"></div>
			</div>
		</div>
	</div>
</div>
<!-- modal quizzes end -->


  <!-- modal quizzes start -->
<div class="modal fade rich-weekly-modal quizzes-modal" role="dialog" data-backdrop="static">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-body">
				<a href="javascript:void(0)" class="btn-close modal-close"  data-dismiss="modal">
					<img src="images/delete-blue.png" alt=""/>
				</a>
				
				 <h1>{{translate('quizz_limit_reach_modal_title')}}</h1>
				<p>{!! translate('quizz_limit_reach_modal_msg',["totalplayed"=>$totalplayed]) !!}</p>
				<button class="btn-form modal-close" data-dismiss="modal">{{translate('cancel_btn')}}</button>
			</div>
		</div>
	</div>
</div>
<!-- modal quizzes end -->

<!-- modal quizz result start -->
<div class="modal fade quizzes-modal" id="quizz_result_modal" role="dialog" >
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-body">
				<a href="javascript:void(0)" class="btn-close modal-close"  data-dismiss="modal">
					<img src="images/delete-blue.png" alt=""/>
				</a>
				<h1 class="quizz-title"></h1>			
			<div id="quizz_result">
				
			</div>
			<div class="clearfix"></div>
			</div>
		</div>
	</div>
</div>
<!-- modal quizz result end -->
