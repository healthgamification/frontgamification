<div class="col-md-9 col-md-push-3 slideInLeft">
			<div class="main-right">

				<!-- post form start -->
				<div class="post-wrapper">
					<form data-title="{{translate('POST_SAVE_TITLE_TXT')}}" data-msg="{{translate('POST_SAVE_MSG_TXT')}}" data-okbtn="{{translate('OK_BTN_TXT')}}" action="{{route('tpedit',app()->getLocale())}}" method="post" autocomplete="off" onsubmit="return submitpostform(this);">
						@csrf
					<div class="post-form-outer">
						<textarea placeholder="{{translate('enter_post_placehosder_txt')}}" name="team_post" data-validation="required" required class="form-control postform-textarea"></textarea>
						<div class="form-error" id="team_post"></div>
						<div class="post-form-action" >
							<button class="btn-form">{{translate('enter_post_send_btn_txt')}}</button>
						</div>
					</div>
				</form>
				</div>
				<!-- post form end -->

				<!-- post details start-->
				<div class="post-detail-wraper">
					<div class="post-detail-collection-view">
						@forelse($posts as $post)
						<div class="post-item">
							<div class="media-img">
								<img src="images/{{$post->member->avatar}}" class="media-image-round">
							</div>
							<div class="media-body">
								<div class="author-info">
									<span class="author-name">{{$post->member->member_name}}</span>&nbsp;
									<p class="author-date">{{time_elapsed_string($post->created_at)}}</p>
								</div>
								<div class="post-content">
									{!! $post->post !!}
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
						@empty
						@endforelse	
						<div id="team_post_page">	
						<div class="col-sm-12">
						<div class="pull-right">						
							{!! $posts->links() !!}	
						</div>
						</div>
						</div>				
					</div>
				</div>
				<!-- post details start-->

			</div>
		</div>

  @include('includes.point')