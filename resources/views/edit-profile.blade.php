<div class="col-md-9 col-md-push-3 slideInLeft">	
			<div class="main-right">

				<!-- edit profile form start -->
				<form data-title="{{translate('PROFILE_UPDATE_TITLE_TXT')}}" data-msg="{{translate('PROFILE_UPDATE_MSG_TXT')}}" data-okbtn="{{translate('OK_BTN_TXT')}}" action="{{route('pedit',app()->getLocale())}}" id="edit_profile_form" method="post" autocomplete="off" onsubmit="return submitform(this); return false;">
					@csrf
				<div class="edit-profile-wrapper">
					<div class="row">
						<div class="col-md-12 form-group">
							<h1 class="text-center">{{translate('EDIT_PROFILE_TXT')}}</h1>
						</div>
						</div>
						<div class="row">
						<div class="col-md-12 form-group">
								<h3>{{translate('CHANGE_AVATAR_TXT')}}</h3>
							</div>
						</div>
						<div class="user-wrap-outer">
							<div class="row">
							<div class="owl-carousel">
							 @forelse($profileimgs as $key=>$img)									
								
									<div class="user-box">
										<div class="user-image change-avatar {{$user->avatar==$img?'active':''}}" for="profileimgsele{{$key}}">
											<img src="images/{{$img}}"  alt="user">
											<input type="radio" {{$user->avatar==$img?'checked':''}} style="display: none;" id="profileimgsele{{$key}}" name="profile_image" value="{{$img}}">
																	
									</div>
								</div>
								@empty
								@endforelse
								<div class="clearfix"></div>
							</div>
						</div>
						</div>
					
						<div class="row">
								<div class="col-md-12 form-group">
										<h3>{{translate('EDIT_NAME_TXT')}}</h3>
									</div>
						<div class="col-md-12 form-group">
							<label>{{translate('FULL_NAME_TXT')}}</label>
							<input type="text" class="form-control" data-validation="required" name="member_name" value="{{old('member_name',$user->member_name)}}"/>
							<div class="form-error" id="member_name"></div>
						</div>
						<!--include('includes.dynamic-field')-->
						<div class="clearfix"></div>
					</div>
					<div class="row">
						<div class="col-md-12 form-group">
							<h3>{{translate('EDIT_PASSWORD_TXT')}}</h3>
						</div>
						<div class="col-md-12 form-group">
							<label>{{translate('CURRENT_PASSWORD_TXT')}}</label>
							<input type="password" name="current_password" class="form-control"/>
							<div class="form-error" id="current_password"></div>
						</div>
						<div class="col-md-12 form-group">
							<label>{{translate('NEW_PASSWORD_TXT')}}</label>
							<input type="password" name="password" class="form-control"/>
							<div class="form-error" id="password"></div>
						</div>
						<div class="col-md-12 form-group">
							<label>{{translate('CONFIRM_PASSWORD_TXT')}}</label>
							<input type="password" name="password_confirmation" class="form-control"/>
							<div class="form-error" id="password_confirmation"></div>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="row">
						<div class="col-md-12 form-group">
							<!-- <button class="btn-form">{{translate('CLOSE_BTN_TXT')}}</button> -->
							<button class="btn-form submitBtn">{{translate('SAVE_BTN_TXT')}}</button>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</form>
				<!-- edit profile form end -->

			</div>
		</div>

@include('includes.point')
<script>$('.owl-carousel').owlCarousel({
    items: 12,
    loop: false,
    nav: true,
    navText: [
      " < ",
      " > "
    ],
    responsive: {
      0: {
        items: 2
      },
      600: {
        items: 3
      },
      1000: {
        items: 6
      }
    }
  });
$(document).ready(function(){
  $.each($(".datepicker"),(k,v)=>{
  	option={changeMonth: true,
      changeYear: true};
  	var input = $(v);
  	if(input.data("format")){
  		option.dateFormat = input.data("format");
  	}
  	if(input.data("mindate")){
  		option.minDate = input.data("mindate");
  	}
  	if(input.data("maxdate")){
  		option.maxDate = input.data("maxdate");
  	}
    input.datepicker(option);
  })
})
</script>

