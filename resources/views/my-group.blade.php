<section class="slideInLeft">
<div class="top-user-wrapper">

			<!-- top user cards start -->
			<div class="user-wrap-outer">
				<div class="row">
					@forelse($myteam->player_by_point_asc() as $player)
					<div class="col-md-2 col-sm-4 col-xs-6">
						<div class="user-box">
							<div class="user-image">
								<img src="images/{{$player->member->avatar}}" alt="user">
							</div>
							<p class="username-text">{{$player->member->member_name}}</p>
						</div>
					</div>
					@empty
					@endforelse
					<div class="clearfix"></div>
				</div>
			</div>
			<!-- top user cards end -->

			<!-- users heading start -->
			<div class="group-heading">
				<h1>{{translate('TEAM_SINGULAR')}} : {{$myteam->name}}</h1>
				<div class="circle-cards-wrapper">
					<div class="card-inner">
						<div class="card-number">#{{Auth::user()->my_team_rank}}</div>
						<div class="card-text">{{translate('TEAM_RANK')}}</div>
					</div>
				</div>
				<div class="circle-cards-wrapper">
					<div class="card-inner">
						<div class="card-number">{{Auth::user()->my_team_point}}</div>
						<div class="card-text">{{translate('TEAM_POINTS')}}</div>
					</div>
				</div>
			</div>
			<!-- users heading end -->

			<!-- progress bar start -->
			<div class="progress-bar-wrapper">
				<h2>{{translate('COMPLETE_CHALLENGE_TXT')}}</h2>
				<div class="progress">
					<span class="circle-start">{{$allplayedchallenge}}</span>
					<span class="circle-end">{{translate('OUT_OFF')}} {{$allchallenge}}</span>
					<div class="progress-bar yellow progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:{{$allchallenge>0?round(($allplayedchallenge/$allchallenge)*100,2):0}}%"></div>
				</div>
			</div>

			<div class="progress-bar-wrapper mb-50">
				<h2>{{translate('COMPLETE_QUIZZ_TXT')}}</h2>
				<div class="progress">
					<span class="circle-start">{{$allplayedquizz}}</span>
					<span class="circle-end">{{translate('OUT_OFF')}} {{$allquizz}}</span>
					<div class="progress-bar pink  progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:{{$allquizz>0?round(($allplayedquizz/$allquizz)*100,2):0}}%"></div>
				</div>
			</div>
			<!-- progress bar end -->

			<!-- activities start -->
			<div class="activities-heading">
				<h3>{{translate('THIS_WEEK_ACTIVITIES')}}</h3>
			</div>
			<!-- activities end -->

			<!-- my group start -->
			<div class="point-table-wrapper panel-group" id="accordion">				
					@forelse($myteam->player_by_point_asc() as $key=>$player)
				<div class="accordion-outer panel">
					<div class="accordion-header" data-toggle="collapse" data-parent="#accordion" data-target="#collapse{{$key}}">
						<div class="header-item-left">
							<div class="user-image">
								<img src="images/{{$player->member->avatar}}" alt="user"/>
							</div>
							<span class="head-name pull-left">{{$player->member->member_name}}</span>
							<span class="clearfix"></span>
						</div>
						<div class="header-item-right">
							<span class="head-count">{{$player->member->my_point}}</span>
							<span class="head-point">{{translate('POINTS')}}</span>
							<button class="head-arrow" ><span class="arrow-icon rotate"> &gt; </span></button>
						</div>
						<div class="clearfix"></div>
					</div>					
					<div id="collapse{{$key}}" class="accordion-body collapse {{$player->member->id==Auth::id()?'in':''}}">
						<div class="user-wrap-outer">
							<div class="row">
								 @forelse($player->member->challenges as $challenge)
								<div class="col-md-2 col-sm-4 col-xs-6">
									<div class="user-box">
										<div class="user-image">
											<img src="images/challange_active.png" alt="user"/>
										</div>
										<p class="username-text">{{$challenge->challenge}}</p>
									</div>
								</div>
								@empty 
                                @endforelse		
                                @forelse($player->member->quizzes as $quizz)						
								<div class="col-md-2 col-sm-4 col-xs-6">
									<div class="user-box">
										<div class="user-image">
											<img src="images/quiz_done.png" alt="user"/>
										</div>
										<p class="username-text">{{$quizz->qsettitle}}</p>
									</div>
								</div>
							@empty 
                                @endforelse		
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
					
				</div>
				@empty
					@endforelse
			</div>
			<!-- my group end -->

		</div>
	</section>