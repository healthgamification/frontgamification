<div class="col-md-9 col-md-push-3 slideInLeft">    
            <div class="main-right">
        
                <div class="progress-bar-wrapper">
                    <h2>{{translate('COMPLETE_CHALLENGE_TXT')}}</h2>
                    <div class="progress">
                        <span class="circle-start">{{Auth::user()->challenges->count()}}</span>
                        <span class="circle-end">{{translate('OUT_OFF')}} {{Auth::user()->no_of_challenge}}</span>
                        <div class="progress-bar yellow progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:{{round((Auth::user()->challenges->count()/Auth::user()->no_of_challenge)*100,2)}}%"></div>
                    </div>
                </div>
                <div class="progress-bar-wrapper">
                    <h2>{{translate('COMPLETE_QUIZZ_TXT')}}</h2>
                    <div class="progress">
                        <span class="circle-start">{{Auth::user()->quizzes->count()}}</span>
                        <span class="circle-end">{{translate('OUT_OFF')}} {{Auth::user()->no_of_quizzes}}</span>
                        <div class="progress-bar pink  progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:{{round((Auth::user()->quizzes->count()/Auth::user()->no_of_quizzes)*100,2)}}%"></div>
                    </div>
                </div>
                <div class="activities-wrapper">
                    <div class="activities-heading">
                        <h3>{{translate('THIS_WEEK_ACTIVITIES')}}</h3>
                    </div>
                    <div class="activity-inner-wrap">
                        <div class="col-md-6 ">
                            <div class="row">
                                <div class="weeklyChallengesWrap">
                                    @forelse(Auth::user()->challenges as $challenge)
                                    <div class="col-md-4 col-xs-6">
                                        <div class="circle-box">
                                        <img src="images/challange_active.png" alt="" />
                                        <p>{{$challenge->challenge}}</p>
                                        </div>
                                    </div>
                                     @empty 
                                     @endforelse
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                        <div class="row">
                            <div class="weeklyQuizzesWrap">

                            @forelse(Auth::user()->quizzes as $quizz)
                                <div class="col-md-4 col-xs-6">
                                    <div class="circle-box">
                                        <img src="images/quiz_done.png" alt="" />
                                        <p>{{$quizz->qsettitle}}</p>
                                    </div>
                                </div>
                            @empty
                            @endforelse
                                <div class="clearfix"></div>
                            </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
        
       @include('includes.point')