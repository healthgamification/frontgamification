$('#menuCloseButton').on('click',function(){

		$('#header nav').slideToggle();
});

/*-- comment send start --*/
$('.postform-textarea').on('focus', function(){
	$('.post-form-action').slideDown();
});

$('.postform-textarea').on('blur', function(){
	$('.post-form-action').slideUp();
}); 

/*-- comment send end --*/

$(document).ready(function(){
  

  /*-- accordion start --*/
$(document).on('click','.accordion-header', function(){
  var has_in = $(this).siblings('.accordion-body').hasClass('in');
  if(has_in == true)
  {
    $(this).find('.arrow-icon').toggleClass('rotate');
    $(this).parents().siblings().find('.arrow-icon').removeClass('rotate');
  }
  else
  {
    $(this).find('.arrow-icon').toggleClass('rotate');
    $(this).parents().siblings().find('.arrow-icon').removeClass('rotate');
  }
});
/*-- accordion end --*/


});

/*-- login start --*/
$('#forgot_password').on('click', function(){
	$('.forgot-box').show();
	$('.login-box').hide();
});
$('#back_login, #send_mail').on('click', function(){
	$('.login-box').show();
	$('.forgot-box').hide();
});
/*-- login end --*/
$(document).ready(function(){
$.validate({
   form : '#loginForm'
 });
 $.validate({
   form : '#forgotForm'
 });

 
 
});


 $(document).on('click','.change-avatar',function(){
var inid= $(this).attr('for');
$('.change-avatar').removeClass('active');
$(this).addClass('active');
$("#"+inid).prop('checked',true);



});



 var lurl = (window.location.href).split("#");
 if(lurl.length>1)
 {
 	var href= lurl[1];
 	var link = $('a[href^="#'+href+'"]')[0];
 	var url = $(link).data('content'); 	
 	var bclass=$(link).data('bclass');
 	loadContent(link,url,bclass); 
 }else{
  var mainurl = lurl[0].toString();
 	if(mainurl.indexOf("login") == -1 && mainurl.indexOf("password") == -1){
    var redirecturl = lurl[0];
 	window.location.href=redirecturl+"#dashboard";
 	var link = $('a[href^="#dashboard"]')[0];
 	var url = $(link).data('content');
 	var bclass=$(link).data('bclass');
 	loadContent(link,url,bclass);
 }
 }

 $(document).on('click','.linkclick a',function(e){
 	var url = $(this).data('content');
 	var bclass=$(this).data('bclass');
 	loadContent(this,url,bclass); 	
 })

 function loadContent(ele,url,bcls){ 
 	$("#dynamic_content").load(url);
 	$('body').attr('class',bcls);
 	$('#header a').removeClass('active');
 	$(ele).addClass('active');
 }


 window.submitform =function(form){
 	var url=$(form).attr('action');
 	var data = $(form).serialize(); 	
 	 $.ajax({
           type: "POST",
           url: url,
           data: data, // serializes the form's elements.
           success: function(data)
           {
               $("#useravatar").attr('src','images/large-avtar/'+data.avatar);
               $("#profilenameview").html(data.member_name);
               $.each($(".form-error"),(k,v)=>{
               	$(v).html('');
               });
               swal({
                    title: $(form).data('title'),
                    text: $(form).data('msg'),
                    icon: "success",
                    button: $(form).data('okbtn'),
                  });
           },
           error:function(res){
           	if(res.responseJSON.errors){
           		var data = res.responseJSON.errors;
           		$.each(Object.keys(data),(k,v)=>{
           			$("#"+v).html('<strong>'+data[v]+'</storng>');
           		})
           	}
           }
         });
 	return false;
 }

 window.submitpostform = function(form){
  var url=$(form).attr('action');
  var data = $(form).serialize(); 
   $.ajax({
           type: "POST",
           url: url,
           data: data, // serializes the form's elements.
           success: function(data)
           {
               swal({
                    title: $(form).data('title'),
                    text: $(form).data('msg'),
                    icon: "success",
                    button: $(form).data('okbtn'),
                  });
               $("#dynamic_content").load("/en/group");

           },
           error:function(res){
            if(res.responseJSON.errors){
              var data = res.responseJSON.errors;
              $.each(Object.keys(data),(k,v)=>{
                $("#"+v).html('<strong>'+data[v]+'</storng>');
              })
            }
           }
         });
  return false;
 }

/*************************** Team Post Pagination *********************/
$(document).on('click', '.pagination a', function (event) {
  event.preventDefault();
   var page = $(this).attr('href').split('page=')[1];
   $("#dynamic_content").load("/en/group?page="+page);
   $('li').removeClass('active');
   $(this).parent().addClass('active');
});


window.resetpassword = function(form){
  var url=$(form).attr('action');
  var data = $(form).serialize(); 
   $.ajax({
           type: "POST",
           url: url,
           data: data, // serializes the form's elements.
           success: function(data)
           {
           $("#successmsg").html(data).show();
           },
           error:function(res){
            if(res.responseJSON.errors){
              var data = res.responseJSON.errors;
              $.each(Object.keys(data),(k,v)=>{
                $("#"+v).html('<strong>'+data[v]+'</storng>');
              })
            }
           }
         });
  return false;
}

var quizz_id ='';
var weekno ='';
var modal;
var timer=90;
var previous_question;
var current_question;
var next_question;
var questions=[];
var quizz_answers={};
var interval;
var submitans=[];
var close_modal_title='';
var close_modal_msg='';
window.showquizmodal = function(ele){
  var quiz_name = $(ele).data("quiz_name");
  var quiz_msg = $(ele).data("quiz_msg");
  quizz_id = $(ele).data("quizz_id");
  weekno = $(ele).data("weekno");
  modal = $("#quizz_start_modal");
   modal.find("#before_start").show();
 modal.find("#after_start").hide();
  modal.find(".quizz-title").html(quiz_name);
  modal.find(".quizz-description-txt").html(quiz_msg);
  modal.find("#quizz_id").val(quizz_id);
modal.modal("show");
close_modal_title=$(ele).data('cancel_modal_title');
close_modal_msg=$(ele).data('cancel_modal_msg');
}

window.start_quizz = function(){
if(quizz_id !='')
{
  modal.find(".modal-close").each((k,v)=>{
    $(v).removeAttr("data-dismiss").attr("onclick","closemodal('quizz_start_modal')");
  })
 modal.find("#before_start").hide();
 getQuestions(function(res){  
  $.each(res,(k,v)=>{
    var html='';
    html+="<li> Q. "+(k+1)+' :- '+v.question;
      html+='<input type="hidden" name="question_id" value="'+v.question_id+'"> <input type="hidden" name="quizz_id" value="'+quizz_id+'"> <input type="hidden" name="weekno" value="'+weekno+'"> <ol type="A" class="answer">'
      $.each(v.answers,(j,l)=>{
        html+='<li class="ans'+j+'"> <input type="radio" onclick="submit_answer('+v.question_id+','+j+');" id="ans'+j+'" name="answer_id" value="'+j+'"><label for="ans'+j+'">'+l+'</label></li>';
      })
      html+='</ol></li>';
      questions.push(html);   
  });
  showquestion(0);
 modal.find("#after_start").show();
 });

 //document.onkeydown = doNotReload;

//  window.onbeforeunload = function() {
//         return "Dude, are you sure you want to leave? Think of the kittens!";
//     }

 }
}

var doNotReload = function(e){
 if(e.keyCode==116)
  {
    return false;
  }
}

window.closemodal = function(ele){
  if(submitans.length>0 && submitans.length<questions.length)
  {
  swal({
  title: close_modal_title,
  text: close_modal_msg,
  icon: "warning",
  buttons: true,
  dangerMode: false,
})
.then((is_close) => {
  if (is_close) {
    $("#"+ele).modal("hide");
    clearInterval(interval);
    $("#dynamic_content").load("/en/quiz");
    clearall();
  } 
});
}else{
  $("#"+ele).modal("hide");
    // clearInterval(interval);
    // if(submitans.length==questions.length+1)
    // {
      $("#dynamic_content").load("/en/quiz");
   // }
    clearall();
}
  $(".modal-backdrop").remove();
      
 
}

function clearall(){
previous_question;
current_question;
next_question;
questions=[];
quizz_answers={};
interval;
submitans=[];
}

function showquestion(current_index){
   clearInterval(interval);
  current_question=current_index;
  next_question= (questions.length-1) == current_index? current_index:current_index+1;
  previous_question =current_index>0?current_index-1:current_index;
  var html = questions[current_question];
  $("#maincontenair").html(html);

  var time=timer;
   $("#timer").html((time<=9?0+''+time:time));
  interval = setInterval(function(){   
    time-=1;   
    $("#timer").html((time<=9?0+''+time:time));
    var percent = ((timer-time)/timer)*100;
    $("#timerprogressbar").css("width",percent+"%");
    if(time==0)
    {
      clearInterval(interval);
       if(current_index<(questions.length-1)){
          submitSingalQuestion();
          showquestion(next_question);
        }else{
          final_quizz_submit();
        }
      }
   }, 1000);
 
}

window.submit_answer = function(question_id,answer_id)
{
   var data = {
    questionid:question_id,
    answer_id:answer_id,
    question_set_id:quizz_id
  }
  quizz_answers[question_id]=data;
  $("#quizznextbtn").attr("disabled","disabled");
  clearInterval(interval);
  submitSingalQuestion();
  var formdata= $("#afterstart_form").serialize();
  $.post("/check",formdata,function(res){
    if(res.correct==answer_id)
    {
      $(".ans"+answer_id).css("background","green");
    }else{
      $(".ans"+answer_id).css("background","red");
      $(".ans"+res.correct).css("background","green");
    }
    setTimeout(function(){
      $("#maincontenair").find("li").each((k,v)=>{
        $(v).hide();
      })
    $("#maincontenair").append("<li class='correctans'>"+res.fact+"</li>");
     $("#quizznextbtn").removeAttr("disabled");
  },1000);
  })
  $("#afterstart_form").find("input[type=radio]").each((k,v)=>{
    $(v).attr("disabled","disabled");
  })
  // if(current_question==next_question)
  // {
  //   $("#buttons").find("button").toggle();
  // }
  
}



window.final_quizz_submit = function() {
    submitSingalQuestion(1);
    $("#before_start").remove();
    $("#after_start").remove();
     clearInterval(interval);
}

function submitSingalQuestion(is_result=0) {
  var responsetime = timer - parseInt($("#timer").html());
  var formdata= $("#afterstart_form").serialize()+"&question_no="+current_question+"&response_time="+responsetime+"&is_result="+is_result;
  if(submitans.indexOf(current_question) == -1){
  submitans.push(current_question);
}

  $.ajax({
           type: "POST",
           url: "/save",
           data: formdata, // serializes the form's elements.
            async:true,
           success: function(data)
           {
            if(is_result==1){
              $("#quizz_result").html(data);             
            }
           },
           error:function(res){
           
           }
         });  
}

window.nextquestion = function(ele){
  submitSingalQuestion();
  showquestion(next_question);
  if(current_question==next_question)
  {
    $(ele).closest("div").find("button").toggle();
  }
}

function getQuestions(cb){
  var data =$("#start_form").serialize();
 $.ajax({
           type: "POST",
           url: "/getquestion",
           data: data, // serializes the form's elements.
           async:true,
           success: function(data)
           {
           cb(data);
           },
           error:function(res){
           
           }
         });
}

var challenge_id;
var weekno;
var challenge_options;
window.showchallengestartmodal = function(ele){
   var challenge_name = $(ele).data("challenge_name");
  var challenge_msg = $(ele).data("challenge_description");
  challenge_id = $(ele).data("challenge_id");
  weekno = $(ele).data("weekno");
  challenge_options = $(ele).data("challenge_options");
  var option_html='';
  if(Object.keys(challenge_options).length>0)
  {
  $.each(Object.keys(challenge_options),(k,v)=>{
    option_html += '<input style="display:none;" type="radio" name="option" id="k'+k+'" value="'+v+'"> <label for="k'+k+'" href="javascript:void(0);" class="btn-question">'+challenge_options[v]+"</label>";
  });
}
  var modal=$("#challenge_start_modal");
  modal.find(".challenge-title").html(challenge_name);
  modal.find(".challenge-msg").html(challenge_msg);
  modal.find("#challenge_id").val(challenge_id);
  modal.find("#challeng_options").html(option_html);
  $(".challenge_option_error_msg").html('');
  modal.modal("show");
}

window.startchallenge = function () {
  var formdata = $("#challenge_start_form").serialize();
  var optionchecked = $("#challenge_start_form").find("input[type=radio]:checked").length;
  if(Object.keys(challenge_options).length>0 && optionchecked ==0)
  {
    $(".challenge_option_error_msg").html("Please select at least one option.");
  }else{
  $.ajax({
           type: "POST",
           url: "/savechallenge",
           data: formdata, // serializes the form's elements.
           async:true,
           success: function(data)
           {
            $("#challenge_start_modal").modal("hide");
           $("#dynamic_content").load("/en/challenge");
           },
           error:function(res){
           
           }
         });
}
}

window.showdailychallengemodal = function(ele){
  var challenge_name = $(ele).data("challenge_name");
  var challenge_msg = $(ele).data("challenge_description");
  challenge_id = $(ele).data("challenge_id");
  var mark= parseInt($(ele).data("mark"));
  var day= parseInt($(ele).data("day"));
  challenge_options = $(ele).data("challenge_options");
  var title=$(ele).data('choose_msg');
  var option_html='';
  if(challenge_options)
  {
   option_html='<div class="horizontal-line"></div> <h4>'+title+':</h4>';
        option_html+='<p> <input type="hidden" name="option" value="'+challenge_options.id+'">'+challenge_options.name+'</p>';
  option_html +='<div class="horizontal-line"></div>';
}
  var modal=$("#challenge_after_start_modal");
  modal.find(".challenge_after_title").html(challenge_name);
  modal.find(".challenge_after_msg").html(challenge_msg);
  modal.find("#afterchallenge_id").val(challenge_id);
  modal.find("#after_start_msg").html(option_html);
  $(".challenge_option_error_msg").html('');
  modal.modal("show");
  var today = $(ele).data('todaydate');
  var currentday = $(ele).data("currentday");
  if(today ==currentday)
  {
    $("#savetodaychallengebtn").attr("disabled","disabled");
  }else{
    $("#savetodaychallengebtn").removeAttr("disabled");
  }
  var perday = $(ele).data('perday');
  var max = (perday*day);
  $('.simple-slider').jRange('setValue',mark);
  $('.simple-slider').jRange('updateMinMaxRange',mark,max);
}

window.savetodaychallenge = function(){
  var formdata = $("#challenge_after_start_form").serialize();
  $.ajax({
           type: "POST",
           url: "/savetodaychallenge",
           data: formdata, // serializes the form's elements.
           async:true,
           success: function(data)
           {
           $("#challenge_after_start_modal").modal("hide");
            $("#dynamic_content").load("/en/challenge");
           },
           error:function(res){
           
           }
         });
}

window.countdown = function(date,ele){
  var ele = $("#"+ele);
  var countDownDate = new Date(date).getTime();
var countdown = setInterval(function() {
  var now = new Date().getTime();
  var distance = countDownDate - now;
  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);
  ele.html(days + " Days " + hours + " Hour "+ minutes + " Minute " + seconds + " Second ");
  if (distance < 0) {
    clearInterval(x);
    ele.html("EXPIRED");
  }
}, 1000);
}

window.showquizzresult = function(ele){
   var quiz_name = $(ele).data("quiz_name");
  var quiz_msg = $(ele).data("quiz_msg");
  var quizz_id = $(ele).data("quizz_id");
  var weekno = $(ele).data("weekno");
  modal = $("#quizz_result_modal");
  modal.find(".quizz-title").html(quiz_name+" Result");
  $.get("/quizzresult/"+quizz_id+"/"+weekno,function(res) {
    modal.find("#quizz_result").html(res);
  })
modal.modal("show");
}

