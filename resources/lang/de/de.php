<?php

return [

    //Login

    "EMAIL"=>"E-mail",
    "PASSWORD"=>"Adgangskode",
    "FORGOT"=>"Glemte",
    "LOGIN"=>"Log på",
    'CHALLENGE_PLURAL'=> 'Udfordringer',
    'CHALLENGE_SINGULAR'=> 'Udfordring',
    'TEAM_PLURAL'=> 'Hold',
    'TEAM_SINGULAR'=> 'Hold',
    'QUIZZ_PLURAL'=> 'Quizzer',
    'QUIZZ_SINGULAR'=> 'Quizzer',
    'WHOS_BEHIND'=> 'Hvem bag sig ?',
    "HOW_TO_PLAY"=>"Sådan spiller du Lifechange ?",
    "HOW_TO_PLAY_DESCRIPTION"=>"Lifechange er et spil, der handler om at prøve at foretage ændringer (såkaldt acking) for at forbedre dit liv i tæt samarbejde med dine kolleger over en periode på 10 uger.",
    "TEAM_DESCRIPTION_TXT"=>"Lifechange fokuserer på gruppedynamikken i at lære og udføre aktiviteterne sammen. Derfor er det vigtigt, at du går sammen med andre eller bringer andre ind i din gruppe, så du sammen fortsætter aktiviteterne for at opnå Lifehacking-mentaliteten.",
    "CHALLENGE_DESCRIPTION_TXT"=>"Udfordringer er udtrykket for de aktiviteter, der kræver en fysisk eller mental ændring på din del. Hver uge kan du og dine teammedlemmer samle op til tre udfordringer, som du hver især fokuserer på. Jo flere udfordringer du administrerer, jo flere point skraber du ind på leaderboardet.",
    "QUIZZ_DESCRIPTION_TXT"=>"Quizer betegnelsen for den viden, du kan lære under spillet gennem spørgsmål om Lifehacking. Du kan tage op til tre quizzer om ugen, og ligesom udfordringerne handler det om at skrabe så mange point som muligt for holdet at rangere på det samlede ledelsesråd.",
    "WHOS_BEHIND_DESCRIPTION_TXT"=>"Lifechange er lavet af UserIT, der i et pilotsamarbejde med Aarhus Kommune, Region Midt, Top Denmark og LEGO vil forsøge at skabe et fundament for sundere og sjovere velfærdstiltag gennem spil til danske virksomheder og institutioner.",

    //Dashboard Menu Management

    "MY_PROFILE"=>"Min profil",
    "MY_GROUP"=>"Min gruppe",
    "GROUP"=>"Gruppe",
    "POINT_TABLE"=>"Punkt tabel",
    "INFO"=>"Info",
    "LOGOUT"=>"Log ud",

    // Left sidebar

    "POINTS"=>"Punkt",
    "TEAM_RANK"=>"Holdrangement",
    "TEAM_POINTS"=>"Holdpoint",
    "EDIT_PROFILE_BTN"=>"Rediger profil",


    //Dashboard

    "COMPLETE_CHALLENGE_TXT"=>"Fuldførte udfordringer i alt",
    "COMPLETE_QUIZZ_TXT"=>"Udførte quizzer i alt",
    "THIS_WEEK_ACTIVITIES"=>"Denne uges aktiviteter",
    "OUT_OFF"=>"UD AF",

    //Info
    "ABOUT_ORGANIZATION"=>"Om organisation",

];
