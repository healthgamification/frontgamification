<?php

return [

    //Login

    "EMAIL"=>"Email",
    "PASSWORD"=>"Password",
    "FORGOT"=>"Forgot",
    "LOGIN"=>"Login",
    'CHALLENGE_PLURAL'=> 'Challenges',
    'CHALLENGE_SINGULAR'=> 'Challenge',
    'TEAM_PLURAL'=> 'Teams',
    'TEAM_SINGULAR'=> 'Team',
    'QUIZZ_PLURAL'=> 'Quizzes',
    'QUIZZ_SINGULAR'=> 'Quizz',
    'WHOS_BEHIND'=> 'Whos behind ?',
    "HOW_TO_PLAY"=>"How to play Lifechange ?",
    "HOW_TO_PLAY_DESCRIPTION"=>"Lifechange is a game that is about trying to make changes (so-called acking) to improve your life in close collaboration with your colleagues over a 10 week period.",
    "TEAM_DESCRIPTION_TXT"=>"Lifechange focus on the group dynamics of learning and performing the activities together. Therefore, it is important that you team-up with others or bring others into your group so that you together keep up the activities to achieve the Lifechanging mentality.",
    "CHALLENGE_DESCRIPTION_TXT"=>"Challenges are the term for those activities that require a physical or mental change onyour part. Each week, you and your team members can pick up to three Challenges that you will each focus on. The more Challenges you manage, the more points you scrape in to the leaderboard.",
    "QUIZZ_DESCRIPTION_TXT"=>"Quiz is the term for the knowledge you can learn during the game through questions about Lifechanging. You can take up to three Quizzes a week, and just like the Challenges, it's about scraping as many points together as possible for the team to rank on the overall leader board.",
    "WHOS_BEHIND_DESCRIPTION_TXT"=>"Lifechange is made by UserIT, which, in a pilot collaboration with Aarhus Municipality, Region Midt, Top Denmark and LEGO, will try to create a foundation for healthier and more fun welfare initiatives through games for Danish companies and institutions.",


    //Dashboard Menu Management

    "MY_PROFILE"=>"My Profile",
    "MY_GROUP"=>"My Group",
    "GROUP"=>"Group",
    "POINT_TABLE"=>"Point Table",
    "INFO"=>"Info",
    "LOGOUT"=>"Logout",

    // Left sidebar

    "POINTS"=>"Points",
    "TEAM_RANK"=>"Team Rank",
    "TEAM_POINTS"=>"Team Points",
    "EDIT_PROFILE_BTN"=>"Edit Profile",


    //Dashboard

    "COMPLETE_CHALLENGE_TXT"=>"Completed Challenges In Total",
    "COMPLETE_QUIZZ_TXT"=>"Completed Quizzes In Total",
    "THIS_WEEK_ACTIVITIES"=>"This Week's Activities",
    "OUT_OFF"=>"OUT OF",

    //Info
    "ABOUT_ORGANIZATION"=>"About Organization",
];