const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .js('resources/js/custom.js', 'public/js')
    .js('resources/js/owl.carousel.min.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
    .sass('resources/sass/style.scss', 'public/css')
    .copyDirectory('resources/images', 'public/images')
    .copy('resources/sass/bootstrap.min.css','public/css/bootstrap.min.css')
    .copy('resources/sass/jquery.range.css','public/css/jquery.range.css')
    .copy('resources/js/jquery.range-min.js','public/js/jquery.range-min.js');
